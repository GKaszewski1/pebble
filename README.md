# pebble

yet another pb clone here, sucks more than original, but sucks less than pbbr... i think...

## Building

```
meson build
ninja -C build
```

## Tooling and commands

### Using address sanitizer

Just set your compiler to clang and add `-Db_sanitize=address`.

**WARNING:** You can use gcc, but expect much worse results when it comes to details, so prefer clang if possible.

```
CC=clang meson build -Db_sanitize=address
```

And build as if nothing has changed.

### Clang-format

If possible, we recommend using an editor with the ability to format files after saving, like Visual Studio Code.

In a modern shell:

```
clang-format src/**.c src/**.h engine/**.c engine/**.h -i
```

### Other useful things

Remove audio: `meson build -Daudio_backend=dummy`

Use clang-tidy: `ninja -C build clang-tidy`
