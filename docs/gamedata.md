# Gamedata

## Glossary

Offset - distance between the beginning of the object and the given element.

## Modes

Gamedata has two modes:

- Static
- Dynamic

Static mode loads the entire file into memory for quick access to assets, while dynamic mode loads the file on demand and makes a copy. When `gamedata_request` is called, the function will return the file. In static mode, path's address and data's address will point to the area in memory where gamedata was loaded. In dynamic mode, addresses will point to the address **with a copy**.

Thus, dynamic mode requires less memory, but is slower. Dynamic mode should be used when memory is low, e.g. 32 MiB. If there is more memory, it is much better to use static mode.

To unload a file just use `gamedata_unload`. In static mode this function will do nothing and return 0.

Dynamic mode also offers four additional functions:

- `gamedata_open`
- `gamedata_read`
- `gamedata_seek`
- `gamedata_tell`

They act as their alternatives: `fopen`, `fread`, `fseek`, `ftell`, with some minor differences.

## Representation

Gamedata consists of three parts:

- Header
- File Table
- File Content

### Header

The header contains the 32-bit number of files in the gamedata, then an offset to the File Content structure.

In C it looks like this:

```c
struct header {
    uint32_t count;
    uint32_t fc_offset;
};
```

### File Table

Each entry in the File Table contains the file size, file path length, path, and offset from beginning of File Content.
In this way, you can avoid unnecessary copying.

The path must be terminated with a null, i.e., '\0'.

In C it looks like this:

```c
struct file {
    uint32_t file_size;
    uint8_t path_length;
    uint8_t path[file_size];
    uint32_t offset;
};
```

### File Content

File Content is raw bytes. They don't have anything special.

## Safety

Safety is very important. We don't want someone's NIC to burn out (this is not a made up story).

Of course, security can be ignored, but the built-in decoder must provide and detect errors when reading gamedata.

Therefore, the built-in parser checks these things:

- Is header bigger than gamedata?
- Is the File Content offset greater than the file size?
- Are we loading more bytes than gamedata has?
- Is the file offset and File Content offset greater than gamedata?

If any of these succeed, the function reading gamedata returns `GD_FILE_CORRUPTED`.

Any idea to secure gamedata is welcome!

## Creating a gamedata

TODO

## Unpacking a gamedata

TODO
