#ifndef ENGINE_AUDIO_H
#define ENGINE_AUDIO_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "gamedata.h"

struct audio_context;
struct audio_chunk;
struct audio_music;

/// @brief Creates an audio context.
/// @return the newly created context, or NULL on failure
struct audio_context *audio_context_create();

/// @brief Cleans up the audio context and all associated resources.
/// @param context the context to clean up
void audio_context_clean(struct audio_context **context);

/// @brief Loads a new Ogg Opus audio chunk from the given path.
/// @param context the audio context
/// @param path the path to the Ogg Opus file
/// @returns the new chunk or NULL on failure
struct audio_chunk *audio_chunk_load_opus(struct audio_context *context, const char *path);

struct audio_chunk *audio_chunk_from_memory_opus(struct audio_context *context, uint8_t *data,
                                                 size_t size);

struct audio_chunk *audio_chunk_from_gamedata_opus(struct audio_context *context,
                                                   struct gamedata *gamedata,
                                                   struct gd_pointer ptr);

/// @brief Cleans up an audio chunk and all associated resources.
/// @param sample the chunk to clean up
void audio_chunk_clean(struct audio_chunk **sample);

struct chunk_play_params {
	struct audio_chunk *chunk;
	uint8_t volume;
};

struct chunk_play_params chunk_play_params_defaults();

/// @brief Starts playback of the chunk.
/// @param chunk the chunk
void audio_chunk_play(struct audio_chunk *chunk);
void audio_chunk_play_ex(struct chunk_play_params *params);

/// @brief Loads music from an Ogg Opus file.
/// @param context the audio context
/// @param path path to the Ogg Opus file
struct audio_music *audio_music_load_opus(struct audio_context *context, const char *path);

struct audio_music *audio_music_from_memory_opus(struct audio_context *context, uint8_t *data,
                                                 size_t size);

struct audio_music *audio_music_from_gamedata_opus(struct audio_context *context,
                                                   struct gamedata *gamedata,
                                                   struct gd_pointer ptr);

/// @brief Cleans up a music chunk and all associated resources.
/// @param music the music chunk to clean up
void audio_music_clean(struct audio_music **music);

/// @brief Plays music.
/// @param music the music to play
/// @param looping whether the music should loop
void audio_music_play(struct audio_music *music, bool looping);

/// @brief Seeks to the given time.
/// @param music the music to seek
/// @param seconds time to seek to in seconds
void audio_music_seek(struct audio_music *music, float seconds);

/// @brief Returns current playback position
float audio_music_get_position(struct audio_music *music);

#endif
