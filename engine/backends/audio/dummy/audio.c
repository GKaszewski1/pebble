#include "backends/audio/audio.h"

#include <stddef.h>
#include <stdlib.h>

// NOTE(legalprisoner8140):
// It was liquid's idea!
#define DUMMY_POINTER ((void *)0x69)

// NOTE(legalprisoner8140):
// In C we cannot have a zero size structure, so we have to give at least one
// field
struct audio_context {
	char _;
};

struct audio_chunk {
	char _;
};

struct audio_music {
	char _;
};

struct audio_context *
audio_context_create()
{
	return DUMMY_POINTER;
}

void
audio_context_clean(struct audio_context **context)
{
	(void)context;
}

void
audio_chunk_clean(struct audio_chunk **chunk)
{
	(void)chunk;
}

void
audio_chunk_play(struct audio_chunk *chunk)
{
	(void)chunk;
}

void
audio_chunk_play_ex(struct chunk_play_params *params)
{
	(void)params;
}

struct audio_chunk *
audio_chunk_load_opus(struct audio_context *context, const char *path)
{
	(void)context;
	(void)path;

	return DUMMY_POINTER;
}

struct audio_chunk *
audio_chunk_from_memory_opus(struct audio_context *context, uint8_t *data, size_t size)
{
	(void)context;
	(void)data;
	(void)size;

	return DUMMY_POINTER;
}

struct audio_chunk *
audio_chunk_from_gamedata_opus(struct audio_context *context, struct gamedata *gamedata,
                               struct gd_pointer ptr)
{
	(void)context;
	(void)gamedata;
	(void)ptr;

	return DUMMY_POINTER;
}

struct audio_music *
audio_music_load_opus(struct audio_context *context, const char *path)
{
	(void)context;
	(void)path;

	return DUMMY_POINTER;
}

struct audio_music *
audio_music_from_memory_opus(struct audio_context *context, uint8_t *data, size_t size)
{
	(void)context;
	(void)data;
	(void)size;

	return DUMMY_POINTER;
}

struct audio_music *
audio_music_from_gamedata_opus(struct audio_context *context, struct gamedata *gamedata,
                               struct gd_pointer ptr)
{
	(void)context;
	(void)gamedata;
	(void)ptr;

	return DUMMY_POINTER;
}

void
audio_music_clean(struct audio_music **music)
{
	(void)music;
}

void
audio_music_play(struct audio_music *music, bool looping)
{
	(void)music;
	(void)looping;
}

void
audio_music_seek(struct audio_music *music, float seconds)
{
	(void)music;
	(void)seconds;
}

float
audio_music_get_position(struct audio_music *music)
{
	(void)music;
	// NOTE(legalprisoner8140):
	// I hope nobody will check it
	return 0.0f;
}
