#include "backends/audio/audio.h"

#include <SDL.h>
#include <SDL_audio.h>
#include <assert.h>
#include <opusfile.h>
#include <stdbool.h>

#include "backends/threads/mutex.h"
#include "common/log.h"
#include "common/serialization.h"
#include "common/util.h"

struct audio_decoder_userdata {
	struct gamedata *gamedata;
	struct gd_pointer ptr;
};

struct playing_chunk {
	bool active;
	size_t read_pos;
	uint8_t volume;
	struct audio_chunk *chunk;
};

struct playing_music {
	bool active;
	bool looping;
	struct audio_music *music;
};

struct audio_context {
	SDL_AudioDeviceID device_id;
	struct mutex *mutex_audio;
	struct playing_chunk *playing_chunks;
	size_t playing_chunks_capacity; // Max count of playing chunks

	struct playing_music playing_music;
};

struct audio_chunk {
	struct audio_context *context;
	int16_t *pcm_data;
	size_t pcm_data_size;
};

struct audio_music {
	struct audio_context *context;
	struct audio_decoder_userdata userdata;
	uint32_t read_samples;
	OggOpusFile *opus_file;
};

struct stereo_sample {
	int16_t left;
	int16_t right;
};

static void audio_music_seek_nonblocking(struct audio_music *music, float seconds);

static void
audio_callback(void *userdata, uint8_t *stream, int bytes)
{
	struct audio_context *context = userdata;
	int16_t *output = (int16_t *)stream;
	uint32_t sample_count = bytes / sizeof(int16_t);
	// struct stereo_sample *output_stereo = output;
	// size_t stereo_sample_count = bytes / sizeof(struct stereo_sample);

	for (size_t i = 0; i < sample_count; i++) {
		output[i] = 0; // Clear buffer
	}

	mutex_lock(context->mutex_audio);

	// Decode music
	if (context->playing_music.active) {
		struct audio_music *music = context->playing_music.music;
		const int channel_count = 2;

		int16_t pcm_buffer[256];

		size_t to_read = sample_count;
		size_t read_pos = 0;

		while (to_read > 0) {
			size_t read_cnt = sizeof(pcm_buffer) / sizeof(int16_t);
			if (read_cnt > to_read)
				read_cnt = to_read;

			int read_samples = op_read_stereo(music->opus_file, pcm_buffer, read_cnt);

			if (read_samples > 0) {
				if (read_samples == OP_HOLE) {
					// Corrupted chunk, stop music
					context->playing_music.active = false;
					break;
				} else {
					// Decoded
					for (int i = 0; i < read_samples * channel_count; i++) {
						output[read_pos + i] = pcm_buffer[i];
					}
					to_read -= read_samples * channel_count;
					read_pos = sample_count - to_read;
					music->read_samples += read_samples;
				}
			} else {
				// End of file
				if (!context->playing_music.looping) {
					// Stop music
					context->playing_music.active = false;
				} else {
					audio_music_seek_nonblocking(music, 0.0f);
				}
				break;
			}
		}
	}

	for (size_t i = 0; i < context->playing_chunks_capacity; i++) {
		struct playing_chunk *p_chunk = &context->playing_chunks[i];
		struct audio_chunk *sample = p_chunk->chunk;
		if (!p_chunk->active)
			continue;

		size_t read_count = sample_count;
		if (p_chunk->read_pos + read_count > sample->pcm_data_size) {
			read_count = sample->pcm_data_size - p_chunk->read_pos;
		}

		for (size_t j = 0; j < read_count; j++) {
			int in = output[j];
			in += (sample->pcm_data[p_chunk->read_pos + j] * (int)p_chunk->volume) / 255;

			// Clip audio
			if (in < -32768)
				in = -32768;
			if (in > 32767)
				in = 32767;

			output[j] = in;
		}

		p_chunk->read_pos += read_count;

		// End of sample, stop it
		if (p_chunk->read_pos >= sample->pcm_data_size) {
			p_chunk->active = false;
		}
	}

	mutex_unlock(context->mutex_audio);
}

// Create audio context.
struct audio_context *
audio_context_create()
{
	SDL_Init(SDL_INIT_AUDIO);

	struct audio_context *context = ecalloc(1, sizeof(struct audio_context));
	context->mutex_audio = mutex_create();

	SDL_AudioSpec spec = {0};
	spec.callback = audio_callback;
	spec.userdata = context;
	spec.channels = 2;
	spec.format = AUDIO_S16;
	spec.freq = 48000;
	spec.samples = 1024;

	SDL_AudioDeviceID id = SDL_OpenAudioDevice(NULL, 0, &spec, NULL, 0);
	context->device_id = id;
	if (id == 0) {
		LOG_ERRORF("Cannot open audio device: %s", SDL_GetError());
		audio_context_clean(&context);
		return NULL;
	}

	// Init playing chunks
	context->playing_chunks_capacity = 32;
	context->playing_chunks =
	    ecalloc(1, context->playing_chunks_capacity * sizeof(struct playing_chunk));

	// Start audio device
	SDL_PauseAudioDevice(id, 0);

	return context;
}

void
audio_context_clean(struct audio_context **_context)
{
	if (!_context)
		return;

	struct audio_context *context = *_context;

	if (context->device_id) {
		SDL_PauseAudioDevice(context->device_id, 1);
		SDL_CloseAudioDevice(context->device_id);
	}

	mutex_free(&context->mutex_audio);

	// Free playing chunks array
	if (context->playing_chunks) {
		free(context->playing_chunks);
		context->playing_chunks = NULL;
	}

	free(context);
	*_context = NULL;
}

void
audio_chunk_clean(struct audio_chunk **chunk)
{
	if (!chunk)
		return;

	struct audio_chunk *c = *chunk;

	if (!c) {
		*chunk = 0;
		return;
	}

	if (c->context) {
		mutex_lock(c->context->mutex_audio);
		for (size_t i = 0; i < c->context->playing_chunks_capacity; i++) {
			struct playing_chunk *p_chunk = &c->context->playing_chunks[i];
			if (p_chunk->active && p_chunk->chunk == *chunk)
				p_chunk->active = false;
		}
	}

	if (c->pcm_data) {
		free(c->pcm_data);
		c->pcm_data = NULL;
		c->pcm_data_size = 0;
	}

	if (c->context) {
		mutex_unlock(c->context->mutex_audio);
	}

	free(*chunk);
	*chunk = NULL;
}

void
audio_chunk_play(struct audio_chunk *chunk)
{
	struct chunk_play_params params = chunk_play_params_defaults();
	params.chunk = chunk;
	audio_chunk_play_ex(&params);
}

void
audio_chunk_play_ex(struct chunk_play_params *params)
{
	assert(params != NULL);

	if (params->chunk == NULL)
		return; // Do nothing

	struct audio_chunk *chunk = params->chunk;
	struct audio_context *context = chunk->context;

	// Find free slot
	for (size_t i = 0; i < context->playing_chunks_capacity; i++) {
		struct playing_chunk *p_chunk = &context->playing_chunks[i];
		if (p_chunk->active)
			continue;

		p_chunk->read_pos = 0;
		p_chunk->chunk = chunk;
		p_chunk->volume = params->volume;
		p_chunk->active = true;
		break;
	}
}

// return The number of bytes successfully read, or a negative value on error.
static int
audio_decoder_opus_read(void *_userdata, unsigned char *_ptr, int _nbytes)
{
	struct audio_decoder_userdata *userdata = _userdata;
	int read = gamedata_read(userdata->gamedata, &userdata->ptr, _ptr, _nbytes);
	return read;
}

static int
audio_decoder_opus_seek(void *_userdata, opus_int64 _offset, int _whence)
{
	struct audio_decoder_userdata *userdata = _userdata;
	int ret = gamedata_seek(&userdata->ptr, _offset, _whence);
	return ret;
}

static opus_int64
audio_decoder_opus_tell(void *_userdata)
{
	struct audio_decoder_userdata *userdata = _userdata;
	opus_int64 ret = gamedata_tell(&userdata->ptr);
	return ret;
}

static int
audio_decoder_opus_close(void *_userdata)
{
	struct audio_decoder_userdata *userdata = _userdata;
	userdata->gamedata = NULL;
	userdata->ptr = (struct gd_pointer){.offset = 0, .pos = 0, .size = 0};
	return 0;
}

static struct audio_chunk *
process_chunk(struct audio_context *context, OggOpusFile *opus_file)
{
	struct audio_chunk *chunk = ecalloc(1, sizeof(struct audio_chunk));

	const int channel_count = 2;

	int16_t *pcm_data = emalloc(0);
	size_t pcm_data_size = 0; // size in int16_t elements

	while (1) {
		int16_t pcm[1024];
		int read_samples = op_read_stereo(opus_file, pcm, sizeof(pcm) / sizeof(int16_t));
		if (read_samples > 0) {
			if (read_samples == OP_HOLE) { // Corrupted chunk, skip
				LOG_ERROR("Opus packet corrupted");
			} else {
				// Decoded
				int samplecount = read_samples * channel_count;
				pcm_data_size += samplecount;
				pcm_data = realloc(pcm_data, pcm_data_size * sizeof(int16_t));
				// Copy new data
				memcpy(pcm_data + pcm_data_size - samplecount, pcm,
				       samplecount * sizeof(int16_t));
			}
		} else {
			// EOF
			break;
		}
	}

	op_free(opus_file);

	chunk->context = context;
	chunk->pcm_data = pcm_data;
	chunk->pcm_data_size = pcm_data_size;

	return chunk;
}

struct audio_chunk *
audio_chunk_load_opus(struct audio_context *context, const char *path)
{
	if (!context)
		return NULL;

	int err;
	OggOpusFile *opus_file = op_open_file(path, &err);

	if (opus_file == NULL || err != 0) {
		LOG_ERRORF("op_open_file failure for file %s", path);
		return NULL;
	}

	return process_chunk(context, opus_file);
}

struct audio_chunk *
audio_chunk_from_memory_opus(struct audio_context *context, uint8_t *data, size_t size)
{
	if (!context)
		return NULL;

	int err;
	OggOpusFile *opus_file = op_open_memory(data, size, &err);

	if (opus_file == NULL || err != 0) {
		LOG_ERROR("op_open_memory failure");
		return NULL;
	}

	return process_chunk(context, opus_file);
}

struct audio_chunk *
audio_chunk_from_gamedata_opus(struct audio_context *context, struct gamedata *gamedata,
                               struct gd_pointer ptr)
{
	if (!context || !gamedata || !GD_POINTER_EXISTS(ptr))
		return NULL;

	struct audio_decoder_userdata userdata;
	userdata.ptr = ptr;
	userdata.gamedata = gamedata;

	OpusFileCallbacks callbacks;
	callbacks.read = audio_decoder_opus_read;
	callbacks.seek = audio_decoder_opus_seek;
	callbacks.tell = audio_decoder_opus_tell;
	callbacks.close = audio_decoder_opus_close;

	int err;
	OggOpusFile *opus_file = op_open_callbacks(&userdata, &callbacks, NULL, 0, &err);

	if (opus_file == NULL || err != 0) {
		LOG_ERROR("op_open_callbacks failure");
		return NULL;
	}

	return process_chunk(context, opus_file);
}

struct audio_music *
audio_music_load_opus(struct audio_context *context, const char *path)
{
	if (!context)
		return NULL;

	struct audio_music *music = ecalloc(1, sizeof(struct audio_music));

	int err;
	music->opus_file = op_open_file(path, &err);

	if (music->opus_file == NULL || err != 0) {
		LOG_ERRORF("op_open_file failure for file %s", path);
		audio_music_clean(&music);
		return NULL;
	}

	music->context = context;

	return music;
}

struct audio_music *
audio_music_from_memory_opus(struct audio_context *context, uint8_t *data, size_t size)
{
	if (!context || !data)
		return NULL;

	struct audio_music *music = ecalloc(1, sizeof(struct audio_music));

	int err;
	music->opus_file = op_open_memory(data, size, &err);

	if (music->opus_file == NULL || err != 0) {
		LOG_ERROR("op_open_memory failure");
		audio_music_clean(&music);
		return NULL;
	}

	music->context = context;

	return music;
}

struct audio_music *
audio_music_from_gamedata_opus(struct audio_context *context, struct gamedata *gamedata,
                               struct gd_pointer ptr)
{
	if (!context || !gamedata || !GD_POINTER_EXISTS(ptr))
		return NULL;

	struct audio_music *music = ecalloc(1, sizeof(struct audio_music));
	music->userdata.ptr = ptr;
	music->userdata.gamedata = gamedata;

	OpusFileCallbacks callbacks;
	callbacks.read = audio_decoder_opus_read;
	callbacks.seek = audio_decoder_opus_seek;
	callbacks.tell = audio_decoder_opus_tell;
	callbacks.close = audio_decoder_opus_close;

	int err;
	music->opus_file = op_open_callbacks(&music->userdata, &callbacks, NULL, 0, &err);

	if (music->opus_file == NULL || err != 0) {
		LOG_ERROR("op_open_callbacks failure");
		audio_music_clean(&music);
		return NULL;
	}

	music->context = context;

	return music;
}

void
audio_music_clean(struct audio_music **_music)
{
	if (!_music)
		return;

	struct audio_music *music = *_music;
	if (!music)
		return;

	struct audio_context *context = music->context;

	if (context) {
		mutex_lock(context->mutex_audio);
		if (context->playing_music.active && context->playing_music.music == music) {
			context->playing_music.active = false;
		}
	}

	if (music->opus_file) {
		op_free(music->opus_file);
		music->opus_file = NULL;
	}

	if (context) {
		mutex_unlock(context->mutex_audio);
	}

	free(music);

	*_music = NULL;
}

void
audio_music_play(struct audio_music *music, bool looping)
{
	if (!music)
		return;

	struct audio_context *context = music->context;

	mutex_lock(context->mutex_audio);
	context->playing_music.music = music;
	context->playing_music.looping = looping;
	context->playing_music.active = true;
	music->read_samples = 0;
	op_pcm_seek(music->opus_file, 0); // Beginning
	mutex_unlock(context->mutex_audio);
}

static void
audio_music_seek_nonblocking(struct audio_music *music, float seconds)
{
	if (!music)
		return;

	music->read_samples = seconds * 48000;
	op_pcm_seek(music->opus_file, music->read_samples);
}

void
audio_music_seek(struct audio_music *music, float seconds)
{
	struct audio_context *context = music->context;
	mutex_lock(context->mutex_audio);
	audio_music_seek_nonblocking(music, seconds);
	mutex_unlock(context->mutex_audio);
}

float
audio_music_get_position(struct audio_music *music)
{
	if (!music)
		return 0.0f;
	return music->read_samples / 48000.0f;
}
