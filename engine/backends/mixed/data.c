#include "data.h"

#include <string.h>

#include "backends/render/render.h"
#include "common/log.h"
#include "common/util.h"
#include "sys/fs.h"

#ifdef _RENDER_BACKEND_sdl2
#include <SDL.h>
#endif

#ifdef DATA_PRIVATE
char *
#else
static inline char *
#endif
_get_data_path()
{
	bool general_data_path_needs_free = false;
	char *general_data_path = NULL;
	char *app_data_dir = NULL;

#if defined(__linux__) || defined(BSD)
	general_data_path = getenv("XDG_DATA_HOME");
#elif defined(_WIN32)
	general_data_path = getenv("AppData");
#endif

	if (general_data_path == NULL) {
		char *home_dir = getenv("HOME");

		general_data_path = dsprintf("%s/.local/share", home_dir);
		general_data_path_needs_free = true;
	}

	app_data_dir = dsprintf("%s/pebble/", general_data_path);

	if (fs_is_valid_path(app_data_dir)) {
		fs_create_dir(app_data_dir);
		LOG_INFO("Created new directory `pebble` in user data dir");
	}

	if (general_data_path_needs_free) {
		free(general_data_path);
	}
	return app_data_dir;
}

char *
get_data_path()
{
#ifdef _RENDER_BACKEND_sdl2
	char *_path = SDL_GetPrefPath(NULL, "pebble");
	char *path = dstrcpy(_path);
	SDL_free(_path);
	return path;
#else
	return _get_data_path();
#endif
}
