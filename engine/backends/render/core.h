#ifndef ENGINE_RENDER_CORE_H
#define ENGINE_RENDER_CORE_H

#include "common/math/rect.h"

void core_init();
void core_quit();

int display_get_resolution(int *w, int *h);

#endif
