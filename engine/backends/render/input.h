#ifndef ENGINE_RENDER_INPUT_H
#define ENGINE_RENDER_INPUT_H

#include <assert.h>
#include <stdbool.h>

#include "backends/render/input_base.h"
#include "backends/render/window.h"
#include "common/log.h"
#include "common/math/vector.h"

void input_poll_events(struct input *input, struct window *window);

void input_tick(struct input *input);

#endif
