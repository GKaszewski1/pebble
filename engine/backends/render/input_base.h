#ifndef INPUT_BASE_H
#define INPUT_BASE_H

#include <stdbool.h>
#include <stdint.h>

#include "common/math/vector.h"

// backend-independent stuff

enum key {
	/// special value used in some places to signify no key
	KEY_NONE,

	/// arrow keys
	KEY_UP,
	KEY_DOWN,
	KEY_LEFT,
	KEY_RIGHT,

	/// action keys
	KEY_JUMP,
	KEY_SHOOT,
	KEY_ENTER,
	KEY_ESC,

	/// cheats
	KEY_CHEAT_DEBUG_MENU,
	KEY_CHEAT_FLYHACK,
	KEY_CHEAT_GODMODE,
	KEY_CHEAT_PHYSICS_VERBOSE_ENTITIES,
	KEY_CHEAT_PHYSICS_VERBOSE_TILES,
	KEY_CHEAT_DISABLE_SHADOWS,
	KEY_CHEAT_LOCATION_LEFT,
	KEY_CHEAT_LOCATION_RIGHT,
	KEY_CHEAT_LOCATION_UP,
	KEY_CHEAT_LOCATION_DOWN,

	KEY_STATS,

	/// value used as array size
	KEY__LAST,
};

enum mouse_button {
	MOUSE_BUTTON_LEFT = 1,
	MOUSE_BUTTON_MIDDLE = 2,
	MOUSE_BUTTON_RIGHT = 3,
	MOUSE_BUTTON_X1 = 4,
	MOUSE_BUTTON_X2 = 5,

	MOUSE_BUTTON__LAST,
};

enum {
	STATE_JUST_PRESSED = 0x1,
	STATE_IS_DOWN = 0x2,
	STATE_JUST_RELEASED = 0x4,
};

typedef uint8_t press_state_t;

typedef void (*input_quit_fn)();

struct joystick_binds {
	enum key dpad_up;
	enum key dpad_down;
	enum key dpad_left;
	enum key dpad_right;

	enum key select;
	enum key start;

	enum key X;
	enum key O;
	enum key TR;
	enum key SQ;

	enum key LB;
	enum key RB;
};

struct input {
	vec2i_t mouse;
	vec2i_t wheel;
	struct joystick_binds joystick_binds;

	// private
	press_state_t key_states[KEY__LAST];
	press_state_t mouse_button_states[MOUSE_BUTTON__LAST];
	input_quit_fn quit_callback;
};

void input_init(struct input *);

inline bool
input_key(struct input *input, enum key key, press_state_t mask)
{
	return (input->key_states[key] & mask) != 0;
}

inline bool
input_mouse_button(struct input *input, enum mouse_button mb, press_state_t mask)
{
	return (input->mouse_button_states[mb] & mask) != 0;
}

inline void
input_on_quit(struct input *input, input_quit_fn callback)
{
	input->quit_callback = callback;
}

void input_tick(struct input *);

#endif
