#include "backends/render/window.h"

#include <SDL.h>

#include "SDL_gamecontroller.h"
#include "SDL_hints.h"
#include "common/util.h"

struct window {
	SDL_Window *window;
	SDL_Renderer *renderer;
	SDL_Texture *streaming_texture;
	uint32_t streaming_texture_width;
	uint32_t streaming_texture_height;
	bool quit;

	// Game controller
	SDL_GameController *controller;

	// Size in pixels
	uint32_t width;
	uint32_t height;

	// Streaming texture display boundary
	struct recti display_boundary;

	int transform_stack_len;
};

struct window *
window_create(const char *title, int width, int height, int flags)
{
	assert(title != NULL);
	struct window *window = ecalloc(1, sizeof(*window));

	// NOTE(olekolek1000):
	// Prevent SDL disabling X11 compositor
	SDL_SetHint(SDL_HINT_VIDEO_X11_NET_WM_BYPASS_COMPOSITOR, "0");

	window->window =
	    SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height,
	                     SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);

	Uint32 renderer_flags = SDL_RENDERER_ACCELERATED;

	if (flags & WINDOW_FLAGS_VSYNC)
		renderer_flags |= SDL_RENDERER_PRESENTVSYNC;

	window->renderer = SDL_CreateRenderer(window->window, -1, renderer_flags);

	window->quit = 0;
	window->streaming_texture = NULL;

	if (SDL_NumJoysticks() >= 1) {
		window->controller = SDL_GameControllerOpen(0);
	} else {
		window->controller = NULL;
	}

	if (window->controller != NULL) {
		if (SDL_GameControllerGetAttached(window->controller) != 1) {
			SDL_GameControllerClose(window->controller);
			window->controller = NULL;
		}
		SDL_GameControllerEventState(SDL_ENABLE);
	}

	// to be set later in window_update function
	window->display_boundary.x = 0;
	window->display_boundary.y = 0;
	window->display_boundary.w = 1;
	window->display_boundary.h = 1;

	return window;
}

void
window_free(struct window *window)
{
	assert(window != NULL);

	SDL_GameControllerClose(window->controller);
	SDL_DestroyRenderer(window->renderer);
	SDL_DestroyWindow(window->window);

	free(window);
}

void
window_handle_events(struct window *window, SDL_Event *e)
{
	(void)e;
	(void)window;
}

void
window_render(struct window *window, struct surface *surface)
{
	assert(surface->pixel_format == SURFACE_PIXEL_FORMAT_RGB24);

	if (window->streaming_texture) {
		// Surface size changed. Regenerate texture
		if (window->streaming_texture_width != surface->width ||
		    window->streaming_texture_height != surface->height) {
			SDL_DestroyTexture(window->streaming_texture);
			window->streaming_texture = NULL;
		}
	}

	// Create texture if not exists
	if (!window->streaming_texture) {
		window->streaming_texture_width = surface->width;
		window->streaming_texture_height = surface->height;
		window->streaming_texture =
		    SDL_CreateTexture(window->renderer, SDL_PIXELFORMAT_RGB24,
		                      SDL_TEXTUREACCESS_STREAMING, surface->width, surface->height);
	}

	// Clear window
	SDL_SetRenderDrawColor(window->renderer, 0, 0, 0, 0); // black
	SDL_RenderClear(window->renderer);

	// Update texture stream
	SDL_UpdateTexture(window->streaming_texture, NULL, surface->pixels, surface->pitch);

	int window_w, window_h;
	SDL_GetWindowSize(window->window, &window_w, &window_h);

	window->width = window_w;
	window->height = window_h;

	// Correct aspect ratio
	float surface_aspect_ratio = surface->width / (float)surface->height;
	float window_aspect_ratio = window_w / (float)window_h;

	SDL_Rect rect;

	if (surface_aspect_ratio < window_aspect_ratio) {
		rect.w = window_h * surface_aspect_ratio;
		rect.h = window_h;
		rect.y = 0;
		rect.x = window_w / 2 - rect.w / 2;
	} else {
		rect.w = window_w;
		rect.h = window_w / surface_aspect_ratio;
		rect.x = 0;
		rect.y = window_h / 2 - rect.h / 2;
	}

	window->display_boundary.x = rect.x;
	window->display_boundary.y = rect.y;
	window->display_boundary.w = rect.w;
	window->display_boundary.h = rect.h;

	SDL_RenderCopy(window->renderer, window->streaming_texture, NULL, &rect);

	SDL_RenderPresent(window->renderer);
}

struct recti
window_get_display_boundary(struct window *window)
{
	assert(window != NULL);

	return window->display_boundary;
}

vec2i_t
window_get_streaming_texture_res(struct window *window)
{
	assert(window != NULL);

	return vec2i(window->streaming_texture_width, window->streaming_texture_height);
}

void
window_set_vsync(struct window *window, bool enable)
{
	assert(window != NULL);

	Uint32 renderer_flags = SDL_RENDERER_ACCELERATED;

	if (enable)
		renderer_flags |= SDL_RENDERER_PRESENTVSYNC;

	window->renderer = SDL_CreateRenderer(window->window, -1, renderer_flags);

	window->streaming_texture = NULL;
}
