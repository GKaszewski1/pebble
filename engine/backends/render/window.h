#ifndef ENGINE_RENDER_WINDOW_H
#define ENGINE_RENDER_WINDOW_H

#include <assert.h>
#include <stdbool.h>

#include "common/graphics/surface.h"
#include "common/log.h"
#include "common/math/vector.h"

enum window_flags { WINDOW_FLAGS_VSYNC = 1 };

struct window;

struct window *window_create(const char *title, int width, int height, int flags);
void window_free(struct window *window);
void window_render(struct window *window, struct surface *surface);
struct recti window_get_display_boundary(struct window *window);
vec2i_t window_get_streaming_texture_res(struct window *window);
void window_set_vsync(struct window *window, bool enable);

#endif
