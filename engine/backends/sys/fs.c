#include "fs.h"

#include <string.h>

char *
fs_path_filename(const char *path)
{
	return strrchr(path, '/') + 1;
}
