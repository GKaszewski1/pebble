#include "surface.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "common/util.h"
#include "common/log.h"

FULL_OPT struct surface *
surface_create_empty(uint32_t width, uint32_t height, enum surface_pixel_format format)
{
	// dont't ever bother
	if (width >= 32768 || height >= 32768)
		return NULL;

	struct surface *surface = ecalloc(1, sizeof(struct surface));

	surface->alpha = 255;
	surface->width = width;
	surface->height = height;
	surface->pixel_format = format;
	if (format == SURFACE_PIXEL_FORMAT_RGB24) {
		surface->blending_mode = SURFACE_BLENDING_MODE_NONE;
	} else {
		surface->blending_mode = SURFACE_BLENDING_MODE_BLEND;
	}

	switch (format) {
	case SURFACE_PIXEL_FORMAT_RGB24: {
		surface->pitch = width * 3;
		break;
	}
	case SURFACE_PIXEL_FORMAT_RGBA32: {
		surface->pitch = width * 4;
		break;
	}
	}

	// Allocate pixels
	if (surface->width > 0 && surface->height > 0)
		surface->pixels = ecalloc(1, surface->pitch * surface->height);
	else
		surface->pixels = NULL;

	return surface;
}

FULL_OPT void
surface_clean(struct surface **_surface)
{
	if (!_surface)
		return;

	struct surface *surface = *_surface;
	if (!surface)
		return;

	if (surface->pixels) {
		free(surface->pixels);
		surface->pixels = NULL;
	}

	free(surface);

	*_surface = NULL;
}

FULL_OPT void
surface_get_pixel(struct surface *surface, uint32_t x, uint32_t y, uint8_t *r, uint8_t *g,
                  uint8_t *b, uint8_t *a)
{
	const int pitch = surface->pitch;
	if (surface->pixel_format == SURFACE_PIXEL_FORMAT_RGB24) {
		*r = ((uint8_t *)surface->pixels)[y * pitch + x * 3 + 0];
		*g = ((uint8_t *)surface->pixels)[y * pitch + x * 3 + 1];
		*b = ((uint8_t *)surface->pixels)[y * pitch + x * 3 + 2];
		*a = 255;
	} else if (surface->pixel_format == SURFACE_PIXEL_FORMAT_RGBA32) {
		*r = ((uint8_t *)surface->pixels)[y * pitch + x * 4 + 0];
		*g = ((uint8_t *)surface->pixels)[y * pitch + x * 4 + 1];
		*b = ((uint8_t *)surface->pixels)[y * pitch + x * 4 + 2];
		*a = ((uint8_t *)surface->pixels)[y * pitch + x * 4 + 3];
	} else {
		abort();
	}
}

extern FULL_OPT ALWAYS_INLINE void surface_get_pixel24(struct surface *surface, uint32_t x, uint32_t y, uint8_t *r, uint8_t *g, uint8_t *b);

FULL_OPT ALWAYS_INLINE void
surface_get_pixel32(struct surface *surface, uint32_t x, uint32_t y, uint8_t *r, uint8_t *g,
                    uint8_t *b, uint8_t *a)
{
	const int pitch = surface->pitch;
	*r = ((uint8_t *)surface->pixels)[y * pitch + x * 4 + 0];
	*g = ((uint8_t *)surface->pixels)[y * pitch + x * 4 + 1];
	*b = ((uint8_t *)surface->pixels)[y * pitch + x * 4 + 2];
	*a = ((uint8_t *)surface->pixels)[y * pitch + x * 4 + 3];
}

/// @brief Get pixel directly, with boundary checking.
FULL_OPT inline void
surface_get_pixel_safe(struct surface *surface, int x, int y, uint8_t *r, uint8_t *g,
                       uint8_t *b, uint8_t *a)
{
	if (x < 0 || y < 0 || x >= (int)surface->width || y >= (int)surface->height)
		return;
	surface_get_pixel(surface, x, y, r, g, b, a);
}

extern FULL_OPT ALWAYS_INLINE void surface_set_pixel24(struct surface *surface, int x, int y, uint8_t r, uint8_t g, uint8_t b);


FULL_OPT static inline void
surface_set_pixel32(struct surface *surface, int x, int y, uint8_t r, uint8_t g, uint8_t b,
                    uint8_t a)
{
	const int pitch = surface->pitch;
	((uint8_t *)surface->pixels)[y * pitch + x * 4 + 0] = r;
	((uint8_t *)surface->pixels)[y * pitch + x * 4 + 1] = g;
	((uint8_t *)surface->pixels)[y * pitch + x * 4 + 2] = b;
	((uint8_t *)surface->pixels)[y * pitch + x * 4 + 3] = a;
}

FULL_OPT static inline void
surface_set_pixel_internal(struct surface *surface, int x, int y, uint8_t r, uint8_t g,
                           uint8_t b, uint8_t a)
{
	if (surface->pixel_format == SURFACE_PIXEL_FORMAT_RGB24) {
		surface_set_pixel24(surface, x, y, r, g, b);
	} else if (surface->pixel_format == SURFACE_PIXEL_FORMAT_RGBA32) {
		surface_set_pixel32(surface, x, y, r, g, b, a);
	}
}

FULL_OPT void
surface_set_pixel(struct surface *surface, int x, int y, uint8_t r, uint8_t g, uint8_t b,
                  uint8_t a)
{
	surface_set_pixel_internal(surface, x, y, r, g, b, a);
}

FULL_OPT void
surface_set_pixel_safe(struct surface *surface, int x, int y, uint8_t r, uint8_t g, uint8_t b,
                       uint8_t a)
{
	if (x < 0 || y < 0 || x >= (int)surface->width || y >= (int)surface->height)
		return;
	surface_set_pixel_internal(surface, x, y, r, g, b, a);
}

FULL_OPT void
surface_blend_pixel(struct surface *surface, int x, int y, uint8_t r, uint8_t g, uint8_t b,
                    uint8_t a)
{
	uint8_t rr, gg, bb, aa;
	surface_get_pixel(surface, x, y, &rr, &gg, &bb, &aa);

	uint32_t dest_r = (((uint32_t)rr) * (255 - a) + ((uint32_t)r) * (a)) / 255;
	uint32_t dest_g = (((uint32_t)gg) * (255 - a) + ((uint32_t)g) * (a)) / 255;
	uint32_t dest_b = (((uint32_t)bb) * (255 - a) + ((uint32_t)b) * (a)) / 255;
	uint32_t dest_a = aa + a;

	if (dest_a > 255)
		dest_a = 255;

	surface_set_pixel_internal(surface, x, y, dest_r, dest_g, dest_b, dest_a);
}

FULL_OPT ALWAYS_INLINE static void
surface_blend_pixel24(struct surface *surface, int x, int y, uint8_t r, uint8_t g, uint8_t b,
                    uint8_t a)
{
	uint8_t rr, gg, bb;
	surface_get_pixel24(surface, x, y, &rr, &gg, &bb);

	uint32_t dest_r = (((uint32_t)rr) * (255 - a) + ((uint32_t)r) * (a)) / 255;
	uint32_t dest_g = (((uint32_t)gg) * (255 - a) + ((uint32_t)g) * (a)) / 255;
	uint32_t dest_b = (((uint32_t)bb) * (255 - a) + ((uint32_t)b) * (a)) / 255;

	surface_set_pixel24(surface, x, y, dest_r, dest_g, dest_b);
}

FULL_OPT static inline void
surface_blend_pixel32(struct surface *surface, int x, int y, uint8_t r, uint8_t g, uint8_t b,
                    uint8_t a)
{
	uint8_t rr, gg, bb, aa;
	surface_get_pixel32(surface, x, y, &rr, &gg, &bb, &aa);

	uint32_t dest_r = (((uint32_t)rr) * (255 - a) + ((uint32_t)r) * (a)) / 255;
	uint32_t dest_g = (((uint32_t)gg) * (255 - a) + ((uint32_t)g) * (a)) / 255;
	uint32_t dest_b = (((uint32_t)bb) * (255 - a) + ((uint32_t)b) * (a)) / 255;
	uint32_t dest_a = aa + a;

	if (dest_a > 255)
		dest_a = 255;

	surface_set_pixel32(surface, x, y, dest_r, dest_g, dest_b, dest_a);
}

FULL_OPT ALWAYS_INLINE static void
surface_blend_pixel24_safe(struct surface *surface, int x, int y, uint8_t r, uint8_t g, uint8_t b,
                    uint8_t a)
{
	uint8_t rr, gg, bb;
	if (x < 0 || y < 0 || x >= (int)surface->width || y >= (int)surface->height)
		return;

	surface_get_pixel24(surface, x, y, &rr, &gg, &bb);

	uint32_t dest_r = (((uint32_t)rr) * (255 - a) + ((uint32_t)r) * (a)) / 255;
	uint32_t dest_g = (((uint32_t)gg) * (255 - a) + ((uint32_t)g) * (a)) / 255;
	uint32_t dest_b = (((uint32_t)bb) * (255 - a) + ((uint32_t)b) * (a)) / 255;

	surface_set_pixel24(surface, x, y, dest_r, dest_g, dest_b);
}

FULL_OPT static inline void
surface_blend_pixel32_safe(struct surface *surface, int x, int y, uint8_t r, uint8_t g, uint8_t b,
                    uint8_t a)
{
	uint8_t rr, gg, bb, aa;
	if (x < 0 || y < 0 || x >= (int)surface->width || y >= (int)surface->height)
		return;

	surface_get_pixel32(surface, x, y, &rr, &gg, &bb, &aa);

	uint32_t dest_r = (((uint32_t)rr) * (255 - a) + ((uint32_t)r) * (a)) / 255;
	uint32_t dest_g = (((uint32_t)gg) * (255 - a) + ((uint32_t)g) * (a)) / 255;
	uint32_t dest_b = (((uint32_t)bb) * (255 - a) + ((uint32_t)b) * (a)) / 255;
	uint32_t dest_a = aa + a;

	if (dest_a > 255)
		dest_a = 255;

	surface_set_pixel32(surface, x, y, dest_r, dest_g, dest_b, dest_a);
}

FULL_OPT inline void
surface_blend_pixel_safe(struct surface *surface, int x, int y, uint8_t r, uint8_t g, uint8_t b,
                         uint8_t a)
{
	uint8_t rr, gg, bb, aa;
	if (x < 0 || y < 0 || x >= (int)surface->width || y >= (int)surface->height)
		return;

	surface_get_pixel(surface, x, y, &rr, &gg, &bb, &aa);

	uint32_t dest_r = (((uint32_t)rr) * (255 - a) + ((uint32_t)r) * (a)) / 255;
	uint32_t dest_g = (((uint32_t)gg) * (255 - a) + ((uint32_t)g) * (a)) / 255;
	uint32_t dest_b = (((uint32_t)bb) * (255 - a) + ((uint32_t)b) * (a)) / 255;
	uint32_t dest_a = aa + a;

	if (dest_a > 255)
		dest_a = 255;

	surface_set_pixel(surface, x, y, dest_r, dest_g, dest_b, dest_a);
}

FULL_OPT static inline void
surface_add_pixel(struct surface *surface, int x, int y, uint8_t r, uint8_t g, uint8_t b,
                  uint8_t a)
{
	uint8_t rr, gg, bb, aa;
	surface_get_pixel(surface, x, y, &rr, &gg, &bb, &aa);

	uint32_t dest_r = rr + (r * a) / 255;
	uint32_t dest_g = gg + (g * a) / 255;
	uint32_t dest_b = bb + (b * a) / 255;
	if (dest_r > 255)
		dest_r = 255;
	if (dest_g > 255)
		dest_g = 255;
	if (dest_b > 255)
		dest_b = 255;

	surface_set_pixel_internal(surface, x, y, dest_r, dest_g, dest_b, aa);
}

FULL_OPT static inline void
surface_add_pixel24(struct surface *surface, int x, int y, uint8_t r, uint8_t g, uint8_t b,
                  uint8_t a)
{
	uint8_t rr, gg, bb;
	surface_get_pixel24(surface, x, y, &rr, &gg, &bb);

	uint32_t dest_r = rr + (r * a) / 255;
	uint32_t dest_g = gg + (g * a) / 255;
	uint32_t dest_b = bb + (b * a) / 255;
	if (dest_r > 255)
		dest_r = 255;
	if (dest_g > 255)
		dest_g = 255;
	if (dest_b > 255)
		dest_b = 255;

	surface_set_pixel24(surface, x, y, dest_r, dest_g, dest_b);
}

FULL_OPT static inline void
surface_add_pixel32(struct surface *surface, int x, int y, uint8_t r, uint8_t g, uint8_t b,
                  uint8_t a)
{
	uint8_t rr, gg, bb, aa;
	surface_get_pixel32(surface, x, y, &rr, &gg, &bb, &aa);

	uint32_t dest_r = rr + (r * a) / 255;
	uint32_t dest_g = gg + (g * a) / 255;
	uint32_t dest_b = bb + (b * a) / 255;
	if (dest_r > 255)
		dest_r = 255;
	if (dest_g > 255)
		dest_g = 255;
	if (dest_b > 255)
		dest_b = 255;

	surface_set_pixel32(surface, x, y, dest_r, dest_g, dest_b, aa);
}

FULL_OPT void
surface_blit(struct surface *source, const struct recti *srcrect, struct surface *dest,
             vec2i_t dst)
{
	if (!source || !dest)
		return;

	int src_start_x, src_start_y, src_width, src_height, diffX, diffY;

	if (srcrect) {
		// Copy values
		src_start_x = srcrect->x;
		src_start_y = srcrect->y;
		src_width = srcrect->w;
		src_height = srcrect->h;

		// Limit boundary
		if (src_start_x + src_width > (int)source->width) // X
			src_width = source->width - src_start_x - 1;

		if (src_start_y + src_height > (int)source->height) // Y
			src_height = source->height - src_start_y - 1;
	} else {
		// Full source region
		src_start_x = 0;
		src_start_y = 0;
		src_width = source->width;
		src_height = source->height;
	}

	// Shift destination by source position
	dst.x -= src_start_x;
	dst.y -= src_start_y;

	// Limit under

	diffX = src_start_x + dst.x; // X
	if (diffX < 0) {
		src_start_x -= diffX;
		src_width += diffX;
	}

	diffY = src_start_y + dst.y; // Y
	if (diffY < 0) {
		src_start_y -= diffY;
		src_height += diffY;
	}

	// Limit over

	diffX = (int)dest->width - (dst.x + (src_start_x + src_width)); // X
	if (diffX < 0) {
		src_width += diffX;
	}

	diffY = (int)dest->height - (dst.y + (src_start_y + src_height)); // Y
	if (diffY < 0) {
		src_height += diffY;
	}

	if (src_width < 0)
		return;
	if (src_height < 0)
		return;

	int loop_start_x = src_start_x;
	int loop_start_y = src_start_y;
	int loop_end_x = src_start_x + src_width;
	int loop_end_y = src_start_y + src_height;
	uint8_t r, g, b, a;

#define LOOP_Y for (int y = loop_start_y; y < loop_end_y; y++)
#define LOOP_X for (int x = loop_start_x; x < loop_end_x; x++)

#define GET_PIXEL24                                \
	surface_get_pixel24(source, x, y, &r, &g, &b); \
	a = 255;

#define GET_PIXEL32 surface_get_pixel32(source, x, y, &r, &g, &b, &a)
#define TARGET_POS  dst.x + x, dst.y + y

#define OP_SET_PIXEL24   surface_set_pixel24(dest, TARGET_POS, r, g, b)
#define OP_SET_PIXEL32   surface_set_pixel32(dest, TARGET_POS, r, g, b, a)

#define OP_BLEND_PIXEL24 surface_blend_pixel24(dest, TARGET_POS, r, g, b, (a * source->alpha) / 255)
#define OP_BLEND_PIXEL32 surface_blend_pixel32(dest, TARGET_POS, r, g, b, (a * source->alpha) / 255)

#define OP_ADD_PIXEL24   surface_add_pixel24(dest, TARGET_POS, r, g, b, (a * source->alpha) / 255)
#define OP_ADD_PIXEL32   surface_add_pixel32(dest, TARGET_POS, r, g, b, (a * source->alpha) / 255)

#define COLORKEY_CHECK                                                                   \
	if (source->key == (uint32_t)((r << 16) | (g << 8) | b)) \
		continue;
	//if (source->key_r == r && source->key_g == g && source->key_b == b)
	//	continue;

#define BLIT_OP(GET, CHECK, OP) \
	LOOP_Y                      \
	{                           \
		LOOP_X                  \
		{                       \
			GET;                \
			CHECK;              \
			OP;                 \
		}                       \
	}

#define IS_RGBA(surf) surf->pixel_format == SURFACE_PIXEL_FORMAT_RGBA32
#define IS_RGB(surf) surf->pixel_format == SURFACE_PIXEL_FORMAT_RGB24

	if (source->keyed) {
		switch (source->blending_mode) {
		default:
		case SURFACE_BLENDING_MODE_NONE:
			if (IS_RGBA(source) && IS_RGBA(dest)) {
				BLIT_OP(GET_PIXEL32, COLORKEY_CHECK, OP_SET_PIXEL32);
			} else if (IS_RGBA(source) && IS_RGB(dest)){
				BLIT_OP(GET_PIXEL32, COLORKEY_CHECK, OP_SET_PIXEL24);
			} else if (IS_RGB(source) && IS_RGBA(dest)) {
				BLIT_OP(GET_PIXEL24, COLORKEY_CHECK, OP_SET_PIXEL32);
			} else {
				BLIT_OP(GET_PIXEL24, COLORKEY_CHECK, OP_SET_PIXEL24);
			}
			break;
		case SURFACE_BLENDING_MODE_BLEND:
			if (IS_RGBA(source) && IS_RGBA(dest)) {
				BLIT_OP(GET_PIXEL32, COLORKEY_CHECK, OP_BLEND_PIXEL32);
			} else if (IS_RGBA(source) && IS_RGB(dest)){
				BLIT_OP(GET_PIXEL32, COLORKEY_CHECK, OP_BLEND_PIXEL24);
			} else if (IS_RGB(source) && IS_RGBA(dest)) {
				BLIT_OP(GET_PIXEL24, COLORKEY_CHECK, OP_BLEND_PIXEL32);
			} else {
				BLIT_OP(GET_PIXEL24, COLORKEY_CHECK, OP_BLEND_PIXEL24);
			}
			break;
		case SURFACE_BLENDING_MODE_ADD:
			if (IS_RGBA(source) && IS_RGBA(dest)) {
				BLIT_OP(GET_PIXEL32, COLORKEY_CHECK, OP_ADD_PIXEL32);
			} else if (IS_RGBA(source) && IS_RGB(dest)){
				BLIT_OP(GET_PIXEL32, COLORKEY_CHECK, OP_ADD_PIXEL24);
			} else if (IS_RGB(source) && IS_RGBA(dest)) {
				BLIT_OP(GET_PIXEL24, COLORKEY_CHECK, OP_ADD_PIXEL32);
			} else {
				BLIT_OP(GET_PIXEL24, COLORKEY_CHECK, OP_ADD_PIXEL24);
			}
			break;
		}
	} else {
		switch (source->blending_mode) {
		default:
		case SURFACE_BLENDING_MODE_NONE:
			if (IS_RGBA(source) && IS_RGBA(dest)) {
				BLIT_OP(GET_PIXEL32, ;, OP_SET_PIXEL32);
			} else if (IS_RGBA(source) && IS_RGB(dest)){
				BLIT_OP(GET_PIXEL32, ;, OP_SET_PIXEL24);
			} else if (IS_RGB(source) && IS_RGBA(dest)) {
				BLIT_OP(GET_PIXEL24, ;, OP_SET_PIXEL32);
			} else {
				BLIT_OP(GET_PIXEL24, ;, OP_SET_PIXEL24);
			}
			break;
		case SURFACE_BLENDING_MODE_BLEND:
			if (IS_RGBA(source) && IS_RGBA(dest)) {
				BLIT_OP(GET_PIXEL32, ;, OP_BLEND_PIXEL32);
			} else if (IS_RGBA(source) && IS_RGB(dest)){
				BLIT_OP(GET_PIXEL32, ;, OP_BLEND_PIXEL24);
			} else if (IS_RGB(source) && IS_RGBA(dest)) {
				BLIT_OP(GET_PIXEL24, ;, OP_BLEND_PIXEL32);
			} else {
				BLIT_OP(GET_PIXEL24, ;, OP_BLEND_PIXEL24);
			}
			break;
		case SURFACE_BLENDING_MODE_ADD:
			if (IS_RGBA(source) && IS_RGBA(dest)) {
				BLIT_OP(GET_PIXEL32, ;, OP_ADD_PIXEL32);
			} else if (IS_RGBA(source) && IS_RGB(dest)){
				BLIT_OP(GET_PIXEL32, ;, OP_ADD_PIXEL24);
			} else if (IS_RGB(source) && IS_RGBA(dest)) {
				BLIT_OP(GET_PIXEL24, ;, OP_ADD_PIXEL32);
			} else {
				BLIT_OP(GET_PIXEL24, ;, OP_ADD_PIXEL24);
			}
			break;
		}
	}

#if 0 // Unobfuscated, universal code
	for (int y = loop_start_y; y < loop_end_y; y++) {
		for (int x = loop_start_x; x < loop_end_x; x++) {
			surface_get_pixel(source, x, y, &r, &g, &b, &a);

			if (source->keyed && source->key_r == r && source->key_g == g && source->key_b == b)
				continue;

			uint32_t target_x = dst.x + x;
			uint32_t target_y = dst.y + y;

			switch (source->blending_mode) {
			default:
			case SURFACE_BLENDING_MODE_NONE: {
				surface_set_pixel(dest, target_x, target_y, r, g, b, a);
				break;
			}
			case SURFACE_BLENDING_MODE_BLEND: {
				surface_blend_pixel(dest, target_x, target_y, r, g, b,
				                    (a * source->alpha) / 255);
				break;
			}
			case SURFACE_BLENDING_MODE_ADD: {
				surface_add_pixel(dest, target_x, target_y, r, g, b, (a * source->alpha) / 255);
				break;
			}
			}
		}
	}
#endif
}

FULL_OPT static void
calc_boundary(struct surface *surface, struct recti *rect, int *start_x, int *start_y,
              int *end_x, int *end_y)
{
	if (rect) {
		*start_x = rect->x;
		*end_x = rect->x + rect->w;
		*start_y = rect->y;
		*end_y = rect->y + rect->h;
	} else {
		*start_x = 0;
		*start_y = 0;
		*end_x = surface->width;
		*end_y = surface->height;
	}

	if (*start_x < 0) {
		*end_x += *start_x;
		*start_x = 0;
	}

	if (*end_x > (int)surface->width) {
		*end_x = (int)surface->width;
	}

	if (*start_y < 0) {
		*end_y += *start_y;
		*start_y = 0;
	}

	if (*end_y > (int)surface->height) {
		*end_y = surface->height;
	}
}

FULL_OPT void
surface_fill(struct surface *dest, struct recti *rect, uint8_t r, uint8_t g, uint8_t b,
             uint8_t a)
{
	int start_x, end_x, start_y, end_y;
	calc_boundary(dest, rect, &start_x, &start_y, &end_x, &end_y);
	if (dest->pixel_format == SURFACE_PIXEL_FORMAT_RGB24) {
		for (int y = start_y; y < end_y; y++) {
			for (int x = start_x; x < end_x; x++) {
				surface_set_pixel24(dest, x, y, r, g, b);
			}
		}
	} else if (dest->pixel_format == SURFACE_PIXEL_FORMAT_RGBA32) {
		for (int y = start_y; y < end_y; y++) {
			for (int x = start_x; x < end_x; x++) {
				surface_set_pixel32(dest, x, y, r, g, b, a);
			}
		}
	}
}

FULL_OPT void
surface_fill_blend(struct surface *dest, struct recti *rect, uint8_t r, uint8_t g, uint8_t b,
                   uint8_t a)
{
	if (a == 0)
		return;
	if (a == 255) {
		surface_fill(dest, rect, r, g, b, a);
		return;
	}
	int start_x, end_x, start_y, end_y;
	calc_boundary(dest, rect, &start_x, &start_y, &end_x, &end_y);
	if (dest->pixel_format == SURFACE_PIXEL_FORMAT_RGB24) {
		for (int y = start_y; y < end_y; y++) {
			for (int x = start_x; x < end_x; x++) {
				surface_blend_pixel24_safe(dest, x, y, r, g, b, a);
			}
		}
	} else if (dest->pixel_format == SURFACE_PIXEL_FORMAT_RGBA32) {
		for (int y = start_y; y < end_y; y++) {
			for (int x = start_x; x < end_x; x++) {
				surface_blend_pixel32_safe(dest, x, y, r, g, b, a);
			}
		}
	}
}

FULL_OPT void
surface_set_colorkey(struct surface *surface, bool enable, uint8_t red, uint8_t green,
                     uint8_t blue)
{
	if (!surface)
		return;
	surface->keyed = enable;
	surface->key = (red << 16 | green << 8 | blue);
	// surface->key_r = red;
	// surface->key_g = green;
	// surface->key_b = blue;
}

FULL_OPT void
surface_set_blending_mode(struct surface *surface, enum surface_blending_mode mode)
{
	if (!surface)
		return;
	surface->blending_mode = mode;
}

FULL_OPT void
surface_blit_flip_x(struct surface *source, struct surface *dest, vec2i_t dst)
{
	struct recti r;
	r.w = 1;
	r.y = 0;
	r.h = source->height;
	for (uint32_t x = 0; x < source->width; x++) {
		r.x = source->width - 1 - x;
		surface_blit(source, &r, dest, vec2i(dst.x + x, dst.y));
	}

	return;
}

FULL_OPT void
surface_blit_flip_y(struct surface *source, struct surface *dest, vec2i_t dst)
{
	struct recti r;
	r.h = 1;
	r.x = 0;
	r.w = source->width;
	for (uint32_t y = 0; y < source->height; y++) {
		r.y = source->height - 1 - y;
		surface_blit(source, &r, dest, vec2i(dst.x, dst.y + y));
	}

	return;
}

void
surface_set_alpha(struct surface *surface, uint8_t alpha)
{
	surface->alpha = alpha;
}
