#include "transform.h"

#include <assert.h>
#include <string.h>

#include "common/graphics/util.h"

void
transform_stack_init(struct transform_stack *s)
{
	memset(s, 0, sizeof(*s));
	s->stack[0] = (struct transform){.translate = {0, 0}, .flip_x = false};
	s->top = 1;
}

void
transform_stack_push(struct transform_stack *s, struct transform t)
{
	assert(s->top < TRANSFORM_STACK_MAX);

	t.translate.x += s->stack[s->top - 1].translate.x;
	t.translate.y += s->stack[s->top - 1].translate.y;

	s->stack[s->top] = t;
	++s->top;
}

void
transform_stack_pop(struct transform_stack *s)
{
	assert(s->top > 0);
	--s->top;
}

void
transformed_blit(struct transform_stack *s, struct surface *source, struct surface *dest,
                 vec2i_t destpos)
{
	struct transform t = s->stack[s->top - 1];

	destpos.x -= t.translate.x;
	destpos.y -= t.translate.y;

	if (t.flip_x)
		surface_blit_flip_x(source, dest, destpos);
	else
		surface_blit(source, NULL, dest, destpos);
}

void 
transformed_fill(struct transform_stack *s, struct color_rgba color, struct surface *dest,
                 struct recti rect)
{
	struct transform t = s->stack[s->top - 1];

	rect.x -= t.translate.x;
	rect.y -= t.translate.y;

	surface_fill_blend(dest, &rect, color.r, color.g, color.b, color.a);
}

void 
transformed_draw_stroke_rect(struct transform_stack *s, struct color_rgba color, 
                             struct surface *dest, struct recti rect)
{
	struct transform t = s->stack[s->top - 1];

	rect.x -= t.translate.x;
	rect.y -= t.translate.y;

	surface_util_draw_stroke_rect(dest, &rect, color);
}

vec2i_t
transform_stack_get_translation(struct transform_stack *s)
{
	return s->stack[s->top - 1].translate;
}
