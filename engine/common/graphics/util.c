#include "util.h"

void 
surface_util_draw_stroke_rect(struct surface *s, struct recti *rect, struct color_rgba color)
{
	struct recti r = recti(rect->x, rect->y, rect->w, 1);
	surface_fill(s, &r, color.r, color.g, color.b, color.a);

	r.y = rect->y + rect->h - 1;
	surface_fill(s, &r, color.r, color.g, color.b, color.a);

	r.y = rect->y;
	r.w = 1;
	r.h = rect->h;
	surface_fill(s, &r, color.r, color.g, color.b, color.a);

	r.x = rect->x + rect->w - 1;
	surface_fill(s, &r, color.r, color.g, color.b, color.a);
}
