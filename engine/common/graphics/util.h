/// @addtogroup graphics
/// @{

#ifndef SURFACE_UTIL_H
#define SURFACE_UTIL_H

#include "../math/rect.h"
#include "color.h"
#include "surface.h"

void surface_util_draw_stroke_rect(struct surface *s, struct recti *rect, struct color_rgba color);

#endif

/// @}
