/// @addtogroup math
/// @{

#ifndef RECT_H
#define RECT_H

#include <stdbool.h>

#include "./vector.h"

struct rectf {
	float x, y, w, h;
};

struct recti {
	int x, y, w, h;
};

#define RECT_CONSTRUCTOR(T, DT)                                       \
	inline struct T T(DT x, DT y, DT w, DT h) {                       \
		return (struct T){                                            \
		    .x = x,                                                   \
		    .y = y,                                                   \
		    .w = w,                                                   \
		    .h = h,                                                   \
		};                                                            \
	} inline struct T T##_sides(DT left, DT top, DT right, DT bottom) \
	{                                                                 \
		return (struct T){                                            \
		    .x = left,                                                \
		    .y = top,                                                 \
		    .w = right - left,                                        \
		    .h = bottom - top,                                        \
		};                                                            \
	}

RECT_CONSTRUCTOR(rectf, float)
RECT_CONSTRUCTOR(recti, int)

#undef RECT_CONSTRUCTOR

#define RECT_SIDES(T, DT, suffix)         \
	inline DT rleft##suffix(struct T r)   \
	{                                     \
		return r.x;                       \
	}                                     \
	inline DT rright##suffix(struct T r)  \
	{                                     \
		return r.x + r.w;                 \
	}                                     \
	inline DT rtop##suffix(struct T r)    \
	{                                     \
		return r.y;                       \
	}                                     \
	inline DT rbottom##suffix(struct T r) \
	{                                     \
		return r.y + r.h;                 \
	}

RECT_SIDES(rectf, float, f)
RECT_SIDES(recti, int, i)

#undef RECT_SIDES

inline bool
rintersect(struct rectf *a, struct rectf *b)
{
	return rleftf(*a) < rrightf(*b) && rleftf(*b) < rrightf(*a) && rtopf(*a) < rbottomf(*b) &&
	       rtopf(*b) < rbottomf(*a);
}

inline bool
rcontains(struct rectf *r, vec2_t p)
{
	return p.x >= r->x && p.y >= r->y && p.x < r->x + r->w && p.y < r->y + r->h;
}

inline struct rectf
to_rectf(struct recti r)
{
	return rectf(r.x, r.y, r.w, r.h);
}

inline struct recti
to_recti(struct rectf r)
{
	return recti(r.x, r.y, r.w, r.h);
}

#endif

/// @}
