/// @addtogroup math
/// @{

#ifndef MATH_UTIL_H
#define MATH_UTIL_H

inline int
clampi(int x, int min, int max)
{
	if (x > max)
		x = max;
	else if (x < min)
		x = min;
	return x;
}

inline float
clampf(float x, float min, float max)
{
	if (x > max)
		x = max;
	else if (x < min)
		x = min;
	return x;
}

inline float
lerpf(float v0, float v1, float t)
{
	return (1 - t) * v0 + t * v1;
}

#endif

/// @}
