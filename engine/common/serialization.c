#include "serialization.h"

extern inline uint8_t fdeserialize_u8(FILE *f);
extern inline uint16_t fdeserialize_u16(FILE *f);
extern inline uint32_t fdeserialize_u32(FILE *f);
extern inline uint64_t fdeserialize_u64(FILE *f);

extern inline void fserialize_u8(FILE *f, uint8_t x);
extern inline void fserialize_u16(FILE *f, uint16_t x);
extern inline void fserialize_u32(FILE *f, uint32_t x);
extern inline void fserialize_u64(FILE *f, uint64_t x);

extern inline uint8_t adeserialize_u8(const uint8_t a[], size_t *p);
extern inline uint16_t adeserialize_u16(const uint8_t a[], size_t *p);
extern inline uint32_t adeserialize_u32(const uint8_t a[], size_t *p);
extern inline uint64_t adeserialize_u64(const uint8_t a[], size_t *p);
