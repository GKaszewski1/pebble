/// @addtogroup utility Utility functions
/// @{

#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <stdlib.h>

#include "string.h"

#ifndef UTIL_NO_RENDER
/// @brief Calculates suitable window width and height,
/// depending on the display's size.
///
/// @param w a pointer filled in with the width, may be NULL
/// @param h a pointer filled in with the height, may be NULL
/// @return 0 on success, negative error code on failure
int adjust_size(int *w, int *h);
#endif

/// @}

void *emalloc(size_t size);
void *ecalloc(size_t nmemb, size_t size);
void *erealloc(void *ptr, size_t size);

#endif
