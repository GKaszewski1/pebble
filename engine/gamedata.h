#ifndef GAMEDATA_H
#define GAMEDATA_H

#include <stdint.h>
#include "stb/stb_ds.h"

#define GD_POINTER_EXISTS(ptr) (ptr.offset != 0 && ptr.size != 0)

enum gamedata_error {
	GD_OK = 0,
	// File doesn't exists or permission denied
	GD_CANNOT_OPEN_FILE = -1,
	/// Gamedata is corrupted
	GD_FILE_CORRUPTED = -2,
	/// `file_size` is less than zero
	GD_INVALID_FILESIZE = -3,
};

enum gamedata_mode { GD_MODE_STATIC = 0, GD_MODE_DYNAMIC = 1 };

struct gd_header {
	uint32_t count;
	// File Content offset
	uint32_t fc_offset;
};

struct gd_content {
	uint8_t *data;
	uint32_t size;
};

struct gd_file {
	const char *path;
	struct gd_content content;
	uint32_t offset;
};

struct gd_pointer {
	uint32_t offset;
	uint32_t pos;
	uint32_t size;
};

struct gamedata {
	struct gd_header header;
	struct file_pair {
		char *key;
		struct gd_file *value;
	} * files;

	// private
	int mode;
	void *priv;
};

/// @brief Loads gamedata from file.
/// @param gd Pointer to gamedata struct, must not be NULL
/// @param mode The mode in which the gamedata will work (static or dynamic)
/// @param path Path to a gamedata file, must not be NULL
/// @return GD_OK on success, error code on failure
/// @see Gamedata
int gamedata_from_file(struct gamedata *gd, enum gamedata_mode mode, const char *path);

/// @brief Loads gamedata from memory.
///
/// The gamedata mode is automatically set to static.
///
/// @param gd Pointer to a gamedata struct, must not be NULL
/// @param data Pointer to a data, must not be NULL
/// @param size Gamedata size
/// @return GD_OK on success, error code on failure
/// @see gamedata_from_file
int gamedata_from_memory(struct gamedata *gd, uint8_t *data, size_t size);

/// @brief Returns a pointer to the requested file's content.
///
/// Function operation varies depending on the mode selected. If the mode is static,
/// it simply returns the content. If the mode is dynamic and the file is not found,
/// it loads it and returns it. Each subsequent reference to the file will not load it (it will
/// be loaded only once). If the file needs to be released, see gamedata_unload.
///
/// @param gd Pointer to a gamedata struct, must not be NULL
/// @param path Path to a file, must not be NULL
/// @return Pointer on success, NULL if file doesn't exists
/// @see gamedata_load, gamedata_unload
struct gd_file *gamedata_request(struct gamedata *gd, const char *path);

/// @brief Unloads a file.
/// @param gd Pointer to a gamedata struct, must not be NULL
/// @param path Path to a file, must not be NULL
/// @return Number of bytes freed, 0 if the file doesn't exist, was not loaded, or the mode is
/// static.
size_t gamedata_unload(struct gamedata *gd, const char *path);

/// @brief Frees a gamedata. Doesn't call free.
/// @param gd Pointer to a gamedata struct, must not be NULL
void gamedata_free(struct gamedata *gd);

/// @brief "Opens" the file in gamedata.
/// @param gd Pointer to a gamedata struct, must not be NULL
/// @param path Path to a file, must not be NULL
/// @return gd_pointer with offset to the file, and it's size
/// @note Panics if gamedata mode isn't GD_MODE_DYNAMIC
struct gd_pointer gamedata_open(struct gamedata *gd, const char *path);

/// @brief Reads file's content to where.
/// @param gd Pointer to a gamedata struct, must not be NULL
/// @param where Pointer to a place in memory where data will be readed to, must not be NULL
/// @return The number of bytes successfully read
/// @note Panics if gamedata mode isn't GD_MODE_DYNAMIC
int gamedata_read(struct gamedata *gd, struct gd_pointer *ptr, uint8_t *where, size_t size);

/// @brief Sets gd_pointer's position
/// @param offset Same as fseek
/// @param whence Same as fseek
/// @return Same as fseek
/// @note Panics if gamedata mode isn't GD_MODE_DYNAMIC
int gamedata_seek(struct gd_pointer *ptr, long offset, int whence);

/// @brief Returns current gd_pointer's position
/// @return Same as ftell
/// @note Panics if gamedata mode isn't GD_MODE_DYNAMIC
long gamedata_tell(struct gd_pointer *ptr);

size_t gamedata_save(struct gd_file *files, size_t count, const char *path);

#endif
