#include "fixed.h"

#include <assert.h>
#include <stdio.h>

#include "common/util.h"
#include "ui/ui.h"

static void
ui_fixed_add_child(struct ui_context *ctx, vec2i_t size)
{
	struct ui_fixed *layout = (struct ui_fixed *)ctx->layout;

	layout->next.y += size.y + layout->child_padding;

	layout->count++;
}

static struct ui_layout *
ui_fixed(vec2i_t pos, int child_padding)
{
	struct ui_fixed *fixed = ecalloc(1, sizeof(struct ui_fixed));

	fixed->rect = recti(pos.x, pos.y, 0, 0);
	fixed->next = vec2i(pos.x, pos.y);
	fixed->add_child_impl = ui_fixed_add_child;
	fixed->child_padding = child_padding;

	return (struct ui_layout *)fixed;
}

void
ui_fixed_begin(struct ui_context *ctx, vec2i_t pos, int child_padding)
{
	assert(ctx != NULL);

	ctx->layout = ui_fixed(pos, child_padding);
}


void
ui_fixed_end(struct ui_context *ctx)
{
	assert(ctx != NULL);
	free(ctx->layout);
	ctx->layout = NULL;
}
