#ifndef UI_FIXED_H
#define UI_FIXED_H

#include "common/math/vector.h"
#include "ui.h"

struct ui_fixed {
	UI_LAYOUT_HEADER
	int child_padding;
	int count;
};

void ui_fixed_begin(struct ui_context *ctx, vec2i_t pos, int child_padding);
void ui_fixed_end(struct ui_context *ctx);

#endif
