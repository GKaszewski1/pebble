#include "image.h"

#include <assert.h>
#include <stddef.h>

void
ui_image_clip(struct ui_context *ctx, struct surface *surf, struct recti *rect)
{
	assert(ctx != NULL);

	if ((!surf) || (!rect))
		return;

	if (ctx->layout) {
		ui_push_cmd(ctx, ui_cmd_draw_surf(surf, ctx->layout->next, rect, false));
		ui_layout_add_child(ctx, vec2i(surf->width, surf->height));
	}
}

void
ui_image(struct ui_context *ctx, struct surface *surf)
{
	ui_image_clip(ctx, surf, NULL);
}
