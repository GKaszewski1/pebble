#ifndef UI_LABEL_H
#define UI_LABEL_H

#include "common/graphics/surface.h"
#include "ui.h"

struct surface *_ui_label_color(struct ui_context *ctx, const char *text, struct color_rgb c);
void _ui_label_pos_from(struct ui_context *ctx, struct surface *surf, vec2i_t pos);
void _ui_label_from(struct ui_context *ctx, struct surface *surf);

void ui_label_outline(struct ui_context *ctx, const char *text, struct color_rgb t,
                      struct color_rgb o);
void ui_label_color(struct ui_context *ctx, const char *text, struct color_rgb c);
void ui_label(struct ui_context *ctx, const char *text);

#endif
