#include "ui.h"

#include <assert.h>
#include <stdio.h>

#include "common/log.h"
#include "common/util.h"

struct ui_style
ui_style(struct surface *style)
{
	struct ui_style s;
	s.style = style;
	return s;
}

struct ui_context *
ui_create(struct surface *screen, struct font *font, struct input *input,
          struct ui_style *style)
{
	struct ui_context *ctx = ecalloc(1, sizeof(struct ui_context));

	ctx->screen = screen;
	ctx->style = style;
	ctx->font = font;
	ctx->layout = NULL;
	ctx->input = input;
	ctx->queue_len = 0;
	ctx->locked = true;

	return ctx;
}

void
ui_begin(struct ui_context *ctx)
{
	assert(ctx != NULL);

	// NOTE(legalprisoner8140):
	// Some widgets create surface. We know how immediate mode works and it doesn't allow us
	// to manage memory. However, we can tell the context whether the surface should be freed.
	// Thanks to this, we can safely free surfaces
	for (int i = 0; i < ctx->queue_len; i++) {
		struct ui_command cmd = ctx->command_queue[i];

		switch (cmd.kind) {
		case DRAW_SURFACE:
			if (cmd.c.surf.clean)
				surface_clean(&cmd.c.surf.surf);
			break;

		default:
			break;
		}
	}

	// NOTE(legalprisoner8140):
	// Layout needs to be free, too.
	free(ctx->layout);
	ctx->layout = NULL;

	ctx->queue_len = 0;
	ctx->locked = false;
}

void
ui_layout_add_child(struct ui_context *ctx, vec2i_t size)
{
	assert(ctx != NULL);

	if (ctx->layout->add_child_impl)
		ctx->layout->add_child_impl(ctx, size);
}

struct ui_command
ui_cmd_draw_surf(struct surface *surf, vec2i_t pos, struct recti *rect, bool clean)
{
	struct ui_command cmd;
	cmd.kind = DRAW_SURFACE;
	cmd.c.surf.surf = surf;
	cmd.c.surf.pos = pos;
	// NOTE(legalprisoner8140):
	// Checking if rect is null is useless because this is what
	// surface_blit does
	cmd.c.surf.rect = rect;
	cmd.c.surf.clean = clean;
	return cmd;
}

struct ui_command
ui_cmd_draw_rect(struct recti rect, struct color_rgba color)
{
	struct ui_command cmd;
	cmd.kind = DRAW_RECT;
	cmd.c.rect.rect = rect;
	cmd.c.rect.color = color;
	return cmd;
}

int
ui_reserve_cmd(struct ui_context *ctx)
{
	assert(ctx != NULL);

	if (ctx->locked)
		return -1;

	// We need to check if queue_len won't be greater than COMMAND_QUEUE_SIZE - 1
	// Thanks zero-based numering
	if (ctx->queue_len >= COMMAND_QUEUE_SIZE - 1)
		return -1;

	return ctx->queue_len++;
}

void
ui_set_cmd(struct ui_context *ctx, struct ui_command cmd, int pos)
{
	assert(ctx != NULL);

	if (ctx->locked)
		return;

	if (pos == -1)
		return;

	ctx->command_queue[pos] = cmd;
}

void
ui_push_cmd(struct ui_context *ctx, struct ui_command cmd)
{
	assert(ctx != NULL);

	// Push is just reserve and set
	int pos = ui_reserve_cmd(ctx);
	ui_set_cmd(ctx, cmd, pos);
}

void
ui_render(struct ui_context *ctx)
{
	assert(ctx != NULL);

	for (int i = 0; i < ctx->queue_len; i++) {
		struct ui_command cmd = ctx->command_queue[i];

		switch (cmd.kind) {
		case DRAW_SURFACE:
			surface_blit(cmd.c.surf.surf, cmd.c.surf.rect, ctx->screen, cmd.c.surf.pos);
			break;

		case DRAW_RECT:
			surface_fill_blend(ctx->screen, &cmd.c.rect.rect, cmd.c.rect.color.r,
			                   cmd.c.rect.color.g, cmd.c.rect.color.b, cmd.c.rect.color.a);
			break;
		}
	}

	ctx->locked = true;
}
