/// @addtogroup assets Assets
/// @{

#ifndef ASSETS_H
#define ASSETS_H

#include <stdbool.h>

#include "assets_gc.h"
#include "backends/audio/audio.h"
#include "backends/render/render.h"
#include "common/graphics/surface.h"
#include "common/log.h"
#include "gamedata.h"
#include "ui/ui.h"

#define PATH_MAX_SIZE 1024

#define OBSTACLES_COUNT   278
#define BACKGROUNDS_COUNT 30
#define MUSIC_COUNT       20

#define DEFAULT_ASSETS_DIR "assets"

struct game;

enum assets_flags {
	/// Load assets from files
	ASSETS_NONE = 0,
	/// Use gamedata instead of loading from files
	ASSETS_GAMEDATA = 1,
	/// Use gamedata for default and files for easy replacement
	ASSETS_MIXED = 2 // unused
};

// need one extra frame for the NULL sentinel at the end of each array
#define MAX_PLAYER_ANIMATION_FRAMES 9
enum player_animation {
	PLAYER_STAND,
	PLAYER_RUN,
	PLAYER_VELOCITY,
	PLAYER_FALL,
	PLAYER_SHOOT,

	PLAYER_ANIMATION__LAST,
};

struct assets {
	struct gamedata *gamedata;
	const char *dir;

	// Surfaces
	struct gc_surface_list *gc_obstacles;
	struct gc_surface_list *gc_backgrounds;

	// Player
	struct surface *player[PLAYER_ANIMATION__LAST][MAX_PLAYER_ANIMATION_FRAMES];
	struct surface *player_bullet;

	// Lanuage select
	struct surface *langselect;
	struct surface *langarrow;

	// Main menu
	struct surface *menu_arrows;
	struct surface *menu_background;
	struct surface *menu_foreground;
	struct surface *menu_pebble_team_logo;
	struct surface *menu_game_logo;

	struct surface *menu_plate;
	struct surface *menu_plate_arrow;

	struct surface *game_shadow_bottom;
	struct surface *game_shadow_right;
	struct surface *game_shadow_bottom_right;

	// Pause play menu
	struct surface *pause_play_menu_arrow;

	// Game
	struct surface *savepoint_shine;

	struct surface *projectiles[7];

	// Locations
	struct location **readonly_locations; // stb ds

	// Fonts
	struct font *main_font;
	struct font *ui_font;

	struct ui_style main_style;

	// Audio
	struct audio_music *music[MUSIC_COUNT];
};

extern struct assets g_assets;

void assets_init(const char *assets_dir, struct game *game, int flags);
void assets_clean();

/// @brief Returns a surface by obstacle id.
/// @param id Obstacle id
/// @return Pointer on success, NULL when id is invalid
struct surface *assets_get_obstacle(int id);

/// @brief Returns a surface by background id.
/// @param id Background id
/// @return Pointer on success, NULL when id is invalid
struct surface *assets_get_background(int id);

/// @brief Called every 1 second in main.c, used for garbage collection
void assets_gc();

struct audio_music *assets_get_music(struct game *game, int id);
void assets_free_music(int id);
void assets_free_music_ptr(struct audio_music *mus);

extern struct surface *(*assets_load_surface)(const char *);
extern struct font *(*assets_load_font)(const char *);
extern struct audio_music *(*assets_load_music)(struct audio_context *, const char *);
extern struct audio_chunk *(*assets_load_audio)(struct audio_context *, const char *);

#endif

/// @}
