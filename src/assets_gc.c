#include "assets_gc.h"

#include <assert.h>

#include "assets.h"
#include "common/util.h"

struct gc_surface_list *
gc_surface_list_init(uint32_t surface_count)
{
	struct gc_surface_list *list = ecalloc(1, sizeof(struct gc_surface_list));
	list->surface_count = surface_count;
	list->surfaces = ecalloc(1, sizeof(struct gc_surface) * surface_count);
	list->timeout_duration = 30;

	return list;
}

void
gc_surface_list_free(struct gc_surface_list **list)
{
	if (!list)
		return;
	if (!*list)
		return;

	struct gc_surface_list *l = *list;

	for (uint32_t i = 0; i < l->surface_count; i++) {
		struct gc_surface *gcsurf = &l->surfaces[i];
		surface_clean(&gcsurf->surf);
	}

	free(l->surfaces);

	free(l);
	*list = NULL;
}

struct surface *
gc_surface_list_get(struct gc_surface_list *list, uint32_t index)
{
	struct gc_surface *gcsurf = &list->surfaces[index];
	if (!gcsurf->surf)
		return NULL;
	gcsurf->last_used = 0;
	return gcsurf->surf;
}

struct surface *
gc_surface_list_load(struct gc_surface_list *list, uint32_t index, const char *path)
{
	struct gc_surface *gcsurf = &list->surfaces[index];
	assert(gcsurf->surf == NULL);

	gcsurf->surf = assets_load_surface(path);
	if (!gcsurf->surf)
		return NULL;

	LOG_INFOF("Loaded surface %s (index %d)", path, index);

	gcsurf->last_used = 0;
	return gcsurf->surf;
}

void
gc_surface_list_gc(struct gc_surface_list *list)
{
	// Increment timer
	for (uint32_t i = 0; i < list->surface_count; i++) {
		struct gc_surface *gcsurf = &list->surfaces[i];
		if (!gcsurf->surf)
			continue;

		gcsurf->last_used++;

		if (gcsurf->last_used > list->timeout_duration) {
			// Surface not used for a longer period of time, free it
			surface_clean(&gcsurf->surf);
			LOG_INFOF("Freed surface %d", i);
		}
	}
}
