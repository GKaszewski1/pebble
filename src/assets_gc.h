#ifndef ASSETS_GC_H
#define ASSETS_GC_H

#include <stdint.h>

struct gc_surface {
	struct surface *surf;
	uint32_t last_used;
};

struct gc_surface_list {
	uint32_t timeout_duration; // Defaults to 30 seconds
	uint32_t surface_count;
	struct gc_surface *surfaces; // array
};

/// @brief Init gc_surface_list
struct gc_surface_list *gc_surface_list_init(uint32_t surface_count);

/// @brief Free gc_surface_list
void gc_surface_list_free(struct gc_surface_list **list);

/// @brief Garbage collect old surface list
/// Should be called every e.g. 1s
void gc_surface_list_gc(struct gc_surface_list *list);

/// @brief Get surface by index
/// @returns NULL if surface is not loaded
struct surface *gc_surface_list_get(struct gc_surface_list *list, uint32_t index);

/// @brief Load surface
/// @returns NULL if file not found
struct surface *gc_surface_list_load(struct gc_surface_list *list, uint32_t index,
                                     const char *path);

#endif
