
#ifndef ABSORBER_H
#define ABSORBER_H

#include "./entity.h"
#include "map/world.h"

struct absorber {
	ENTITY_HEADER

	struct physics_body *body;
};

struct absorber *absorber_create(struct world *world, vec2_t global_pos);

#endif
