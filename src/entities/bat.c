#include "bat.h"

#include "player.h"
#include "physics/collision_groups.h"

#define OFFSET 3.0f

static void
clean(struct bat *b)
{
	b->body->remove = true;
}

static void
render(struct bat *b, struct surface *s, float step)
{
	struct surface *sprite = assets_get_obstacle(ID_BAT);

	entity_util_draw_sprite(b->world->game, s, b->body, sprite, vec2i(-OFFSET, -OFFSET), step);
}

static void
update(struct bat *b)
{
	mul2f(b->vel, 0.9f);

	// For now, let's only support one player.
	struct player *p = b->world->players[0];
	vec2_t ppos = p->body->position;
	vec2_t bpos = b->body->position;

	if (ppos.x > bpos.x)
		b->vel.x += 0.1f;
	if (ppos.x < bpos.x)
		b->vel.x -= 0.1f;
	if (ppos.y > bpos.y)
		b->vel.y += 0.1f;
	if (ppos.y < bpos.y)
		b->vel.y -= 0.1f;

	b->vel.y += ((rand() % 10) - 5) * 0.1f;

	b->body->position.x += b->vel.x;
	b->body->position.y += b->vel.y;

	// TODO: sound
}

static void
collide_with_player(struct physics_body *bp, struct physics_body *bc)
{
	(void)bp;

	struct player *p = (struct player *)bc->user;

	player_kill(p);
}

struct bat *
bat_create(struct world *world, vec2_t global_pos)
{
	struct bat *b = ecalloc(1, sizeof(*b));

	entity_init(b, world, render, update, clean);

	b->body = physics_body_create(world->space);
	b->body->position = add2f(global_pos, OFFSET);
	b->body->size = vec2(17, 7);
	b->body->user = b;
	
	b->hp = 3;
	b->tick = 0;

	physics_body_body_callback(b->body, collide_with_player, BODY_COLLISION_PLAYER);

	return b;
}
