#ifndef BLUE_BRICK_H
#define BLUE_BRICK_H

#include "./entity.h"
#include "map/world.h"

struct blue_brick {
	ENTITY_HEADER

	struct physics_body *body;

	vec2i_t location_pos;
	vec2i_t obstacle_pos;
	int hp;
};

struct blue_brick *blue_brick_create(struct world *world, vec2i_t location_pos,
                                     vec2i_t obstacle_pos);

#endif
