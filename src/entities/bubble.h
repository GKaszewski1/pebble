#ifndef BUBBLE_H
#define BUBBLE_H

#include "./entity.h"

#include <stdbool.h>

#include "map/world.h"

struct bubble {
	ENTITY_HEADER

	/// Body for player
	struct physics_body *body_fp;
	
	/// Body for obstacle
	struct physics_body *body_fo;

	bool fall;
	int tick;
	vec2i_t location_pos;
	vec2i_t obstacle_pos;
	float falling_pos;
};

struct bubble *bubble_create(struct world *world, vec2i_t location_pos,
                             vec2i_t obstacle_pos);

#endif
