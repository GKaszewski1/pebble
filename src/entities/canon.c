#include "canon.h"

#include "assets.h"
#include "map/obstacle_data.h"
#include "projectile.h"

static void
bullet_update(struct projectile *p)
{
	++p->age;
	p->remove = p->remove || p->age >= p->lifetime;
}

static void
clean(struct canon *c)
{
	c->body->remove = true;
}

static void
render(struct canon *c, struct surface *s, float step)
{
	struct surface *sprite = assets_get_obstacle(ID_CANON_LEFT + c->type);

	vec2i_t offset;
	switch (c->type) {
	case CANON_LEFT:
		offset = vec2i(c->offset, 0.0f);
		break;
	case CANON_UP:
		offset = vec2i(0.0f, c->offset);
		break;
	case CANON_RIGHT:
		offset = vec2i(-c->offset, 0.0f);
		break;
	case CANON_DOWN:
		offset = vec2i(0.0f, -c->offset);
		break;
	}

	entity_util_draw_sprite(c->world->game, s, c->body, sprite, offset, step);
}

static void
update(struct canon *c)
{
	c->tick++;

	c->offset *= 0.9f;

	if (c->offset < 1.0f)
		c->offset = 0.0f;

	if (c->tick % 60 == 0) {
		struct projectile *p = 
			projectile_create(c->world, c->body->position, c->bullet_vel,
			                  vec2(10.0f, 10.0f), PROJECTILE_TARGET_PLAYER, 
							  g_assets.projectiles[6]);

		p->update_impl = (entity_update_fn)bullet_update;

		world_spawn_entity(c->world, p);

		c->offset = 10.0f;
	}
}

struct canon *
canon_create(struct world *world, vec2_t global_pos, int type)
{
	struct canon *c = ecalloc(1, sizeof(*c));

	if (type >= CANON_LEFT && type <= CANON_DOWN)
		entity_init(c, world, render, update, clean);

	c->body = physics_body_create(world->space);
	c->body->position = global_pos;
	c->body->size = vec2(10, 10);
	c->body->user = c;

	c->type = type;
	c->tick = 0.0f;
	c->offset = 0.0f;

	switch (type) {
	case CANON_LEFT:
		c->bullet_vel = vec2(-3.5f, 0.0f);
		break;
	case CANON_UP:
		c->bullet_vel = vec2(0.0f, -3.5f);
		break;
	case CANON_RIGHT:
		c->bullet_vel = vec2(3.5f, 0.0f);
		break;
	case CANON_DOWN:
		c->bullet_vel = vec2(0.0f, 3.5f);
		break;
	}

	return c;
}
