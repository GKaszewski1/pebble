#include "entity.h"

#include <assert.h>
#include <string.h>

#include "common/util.h"

void
entity_init__impl(struct entity *ent, struct world *world, entity_render_fn render_impl,
                  entity_update_fn update_impl, entity_clean_fn clean_impl)
{
	assert(ent != NULL);
	assert(world != NULL);
	ent->world = world;
	ent->remove = false;
	ent->remove_offscreen = true;
	ent->render_impl = render_impl;
	ent->update_impl = update_impl;
	ent->clean_impl = clean_impl;
}

void
entity_render__impl(struct entity *ent, struct surface *s, float step)
{
	if (ent->render_impl) {
		ent->render_impl(ent, s, step);
	}
}

void
entity_update__impl(struct entity *ent)
{
	if (ent->update_impl) {
		ent->update_impl(ent);
	}
}

void
entity_clean__impl(struct entity *ent)
{
	if (ent->clean_impl) {
		ent->clean_impl(ent);
	}

	free(ent);
}
