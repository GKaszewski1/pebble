#include "falling_block.h"

#include "common/graphics/surface.h"
#include "map/location.h"
#include "map/world.h"
#include "map/obstacle_data.h"

static void
update(struct falling_block *e)
{
	struct location *loc = world_get_location(e->world, e->location_pos);
	if (!loc) {
		goto remove;
	}

	if (e->ticks % 6 == 0) {
		vec2i_t cur_pos = e->obstacle_pos;
		vec2i_t below_pos = vec2i(e->obstacle_pos.x, e->obstacle_pos.y + 1);

		struct obstacle *obstacle_cur = location_get_obstacle(loc, cur_pos);

		obstacle_cur->flags = FLAGS_IS_SOLID | FLAGS_CASTS_SHADOW;

		struct obstacle *obstacle_below = location_get_obstacle(loc, below_pos);

		if (!obstacle_cur || !obstacle_below) {
			goto remove;
		}

		if ((obstacle_below->flags & FLAGS_IS_SOLID) == 0 && 
		    obstacle_below->id != ID_FALLING_BLOCK) {
			// Copy obstacle below us, we'll swap it later
			e->row[e->obstacle_pos.y + 1] = *obstacle_below;

			location_move_obstacle(loc, cur_pos, below_pos);
			e->obstacle_pos.y++;

			// After we've fallen, let's bring the obstacle back
			if (e->row[e->obstacle_pos.y - 1].id != ID_NONE) {
				location_set_obstacle(loc, vec2i(e->obstacle_pos.x, e->obstacle_pos.y - 1), 
				                      e->row[e->obstacle_pos.y - 1]);
			}
		}
	}

	e->ticks++;

	return;

remove:
	e->remove = true;

	// Clear obstacle data
	struct obstacle *obstacle_cur = location_get_obstacle(loc, e->obstacle_pos);

	if (obstacle_cur)
		*obstacle_cur = obstacle_null();

	return;
}

struct falling_block *
falling_block_create(struct world *world, vec2i_t location_pos, vec2i_t obstacle_pos)
{
	struct falling_block *e = ecalloc(1, sizeof(struct falling_block));

	entity_init(e, world, NULL, update, NULL);

	e->location_pos = location_pos;
	e->obstacle_pos = obstacle_pos;

	for (size_t i = 0; i < LOCATION_HEIGHT; i++) {
		e->row[i] = obstacle_null();
	}

	return e;
}
