#ifndef FALLING_BLOCK_H
#define FALLING_BLOCK_H

#include "entity.h"
#include "map/world.h"
#include "physics/physics.h"

struct falling_block {
	ENTITY_HEADER

	vec2i_t location_pos;
	vec2i_t obstacle_pos;
	uint32_t ticks;

	struct obstacle row[LOCATION_HEIGHT];
};

struct falling_block *falling_block_create(struct world *world, vec2i_t location_pos,
                                           vec2i_t obstacle_pos);

#endif
