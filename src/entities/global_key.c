#include "global_key.h"

#include "./util.h"
#include "map/obstacle_data.h"
#include "physics/collision_groups.h"

static void
render(struct global_key *k, struct surface *s, float step)
{
	struct surface *sprite = assets_get_obstacle(ID_KEY1 + k->number);
	entity_util_draw_sprite(k->world->game, s, k->body, sprite, vec2i(0, 0), step);
}

static void
clean(struct global_key *k)
{
	k->body->remove = true;
}

static void
collide_with_player(struct physics_body *bk, struct physics_body *bplayer)
{
	(void)bplayer;

	struct global_key *k = (struct global_key *)bk->user;

	// TODO: issue #28 key acquired animation

	k->world->keys_collected = k->number + 1;
	k->world->game->gamesave->keys_collected = k->number + 1;
	k->remove = true;
}

struct global_key *
global_key_create(struct world *world, vec2_t position, uint8_t number)
{
	struct global_key *k = ecalloc(1, sizeof(*k));

	entity_init(k, world, render, NULL, clean);

	k->body = physics_body_create(world->space);
	k->body->position = position;
	k->body->size = vec2(20, 20);
	k->body->user = k;
	k->number = number;

	physics_body_body_callback(k->body, collide_with_player, BODY_COLLISION_PLAYER);

	return k;
}
