#include "green_ball.h"

#include "map/obstacle_data.h"
#include "physics/collision_groups.h"
#include "player.h"
#include "projectile.h"

static void
clean(struct green_ball *g)
{
	g->remove = true;
	g->body->remove = true;
}

static void
render(struct green_ball *g, struct surface *s, float step)
{
	struct surface *sprite = assets_get_obstacle(ID_GREEN_BALL);

	entity_util_draw_sprite(g->world->game, s, g->body, sprite, vec2i(0, 0), step);
}

static void
update(struct green_ball *g)
{
	struct player *player = g->world->players[0];

	if (g->body->position.x > player->body->position.x && g->body->velocity.x > -1.0f) {
		g->body->velocity.x -= 0.1f;
	}
	
	if (g->body->position.x < player->body->position.x && g->body->velocity.x < 1.0f) {
		g->body->velocity.x += 0.1f;
	}

	g->body->velocity.y += 0.1f;

	if (physics_body_collision(g->body, WALL_LEFT, IS_COLLIDING)) {
		g->body->velocity.x = -2.0f;
		g->body->velocity.y = -3.0f;
	} else if (physics_body_collision(g->body, WALL_RIGHT, IS_COLLIDING)) {
		g->body->velocity.x = 2.0f;
		g->body->velocity.y = -3.0f;
	}

	if (physics_body_collision(g->body, WALL_TOP, IS_COLLIDING)) {
		g->body->velocity.y = -1.0f;
	}
}

static void
collide_with_player(struct physics_body *bp, struct physics_body *bc)
{
	(void)bp;

	struct player *p = (struct player *)bc->user;
	player_kill(p);
}

static void
collide_with_projectile(struct physics_body *bp, struct physics_body *bc)
{
	struct green_ball *g = (struct green_ball *)bp->user;
	struct projectile *p = (struct projectile *)bc->user;

	if (!(p->target == PROJECTILE_TARGET_ENEMIES))
		return;

	g->remove = true;

	projectile_destroy(p);
}

struct green_ball *
green_ball_create(struct world *world, vec2_t global_pos)
{
	struct green_ball *g = ecalloc(1, sizeof(*g));

	entity_init(g, world, render, update, clean);

	g->body = physics_body_create(world->space);
	g->body->position = global_pos;
	g->body->size = vec2(10, 10);
	g->body->user = g;

	physics_body_body_callback(g->body, collide_with_player, BODY_COLLISION_PLAYER);
	physics_body_body_callback(g->body, collide_with_projectile, BODY_COLLISION_PROJECTILES);

	return g;
}
