#ifndef GREEN_BALL_H
#define GREEN_BALL_H

#include "./entity.h"
#include "map/world.h"

struct green_ball {
	ENTITY_HEADER

	struct physics_body *body;
};

struct green_ball *green_ball_create(struct world *world, vec2_t global_pos);

#endif
