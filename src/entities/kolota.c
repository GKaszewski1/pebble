#include "kolota.h"

#include <math.h>

#include "common/timing.h"
#include "map/obstacle_data.h"
#include "map/world.h"
#include "physics/collision_groups.h"
#include "player.h"
#include "projectile.h"

#define KOLOTA_HP 100

static void
spawn_kinetic(struct location *loc)
{
	if (!loc)
		return;

	for (int i = 0; i < 10; i++) {
		struct obstacle ob = (struct obstacle){.id = ID_KINETIC_UP};

		location_set_obstacle(loc, vec2i(1, i), ob);
		location_set_obstacle(loc, vec2i(2, i), ob);
	}
}

static void
bullet_update(struct projectile *p)
{
	++p->age;
	p->remove = p->remove || p->age >= p->lifetime;
}

static void
clean(struct kolota *k)
{
	k->body->remove = true;

	// After defeating him, they should be kinetics in the location to be able to get out
	spawn_kinetic(world_get_location(k->world, k->location_pos));
}

static void
render(struct kolota *k, struct surface *s, float step)
{
	struct surface *sprite = assets_get_obstacle(ID_KOLOTA);

	entity_util_draw_sprite(k->world->game, s, k->body, sprite, vec2i(0, 0), step);

	entity_draw_hp_bar(k->hp, s);

	struct recti lava_pos = recti(k->lava->position.x, k->lava->position.y, 
	                              k->lava->size.x, k->lava->size.y);

	transformed_fill(&k->world->game->tstack, color_rgba(255, 0, 0, 255), s, lava_pos);
}

static void
update(struct kolota *k)
{
	k->tick++;

	// Motion
	k->offsetX = sinf(k->tick * 0.06f) * 20.0f;
	k->body->velocity.y += 0.1f;

	if (physics_body_collision(k->body, WALL_TOP, IS_COLLIDING)) {
		k->body->velocity.y = -1.5f;
	}

	// Bullets
	if (k->tick % 10 == 0) {
		// TODO: sounds (sample 4)
		float velY = sinf(get_ticks() / 400.0f) * 2.0f;
		vec2_t pos = vec2(k->body->position.x + k->offsetX + 20, k->body->position.y + 20);

		struct projectile *p = 
			projectile_create(k->world, pos, vec2(2.5f, velY), vec2(20, 10), 
			                  PROJECTILE_TARGET_PLAYER, g_assets.projectiles[0]);

		// A little bit of magic
		p->update_impl = (entity_update_fn)bullet_update;
		p->lifetime *= 1.5;

		world_spawn_entity(k->world, p);
	}

	if (k->hp < 50 && k->tick % 200 == 0) {
		// TODO: laser
	}

	if (k->hp < 30 && k->tick % 11 == 0) {
		// TODO: sounds
		k->lava->position.y--;
		k->lava->size.y++;
	}

	k->body->position.x = k->pos.x + k->offsetX;
}

static void
collide_with_player(struct physics_body *bp, struct physics_body *bc)
{
	(void)bp;

	struct player *p = (struct player *)bc->user;

	player_kill(p); 
}

static void
collide_with_projectile(struct physics_body *bp, struct physics_body *bc)
{
	struct kolota *k = (struct kolota *)bp->user;
	struct projectile *p = (struct projectile *)bc->user;

	if (!(p->target == PROJECTILE_TARGET_ENEMIES))
		return;

	p->age = p->lifetime;
	k->hp--;

	// TODO: sounds

	if (k->hp <= 0) {
		k->remove = true;
		k->world->game->gamesave->kolota_defeated = 1;
	}
}

static void
lava_collide_with_player(struct physics_body *bp, struct physics_body *bc)
{
	(void)bp;

	struct player *p = (struct player *)bc->user;

	player_kill(p); 
}

struct kolota *
kolota_create(struct world *world, vec2_t global_pos, vec2i_t location_pos)
{
	struct kolota *k = ecalloc(1, sizeof(*k));

	entity_init(k, world, render, update, clean);

	k->body = physics_body_create(world->space);
	k->body->position = global_pos;
	k->body->size = vec2(40, 40);
	k->body->user = k;

	k->hp = KOLOTA_HP;
	k->offsetX = 0;
	k->tick = 0;
	k->pos = global_pos;
	k->location_pos = location_pos;

	// For lava, we need a separate body because we need to somehow know 
	// when a player falls into it
	k->lava = physics_body_create(world->space);
	// A funny way to align lava
	k->lava->position = world_get_global_pos(location_pos, vec2i(0, 0));
	k->lava->position.y += 180.0f;
	k->lava->size = vec2(320, 0);
	k->lava->user = k;

	physics_body_body_callback(k->body, collide_with_player, BODY_COLLISION_PLAYER);
	physics_body_body_callback(k->body, collide_with_projectile, BODY_COLLISION_PROJECTILES);

	physics_body_body_callback(k->lava, lava_collide_with_player, BODY_COLLISION_PLAYER);

	return k;
}
