#include "neon.h"

#include "map/obstacle_data.h"
#include "physics/collision_groups.h"
#include "player.h"
#include "projectile.h"

static void
clean(struct neon *n)
{
	n->body->remove = true;
}

static void
render(struct neon *n, struct surface *s, float step)
{
	struct surface *sprite = assets_get_obstacle(ID_NEON);
	surface_set_colorkey(sprite, true, 0, 0, 0);

	// Flipping le fishe au chocolat
	transform_stack_push(&n->world->game->tstack,
	                     (struct transform){.translate = vec2i(0, 0), .flip_x = !n->right});
	entity_util_draw_sprite(n->world->game, s, n->body, sprite, vec2i(0, 0), step);
	transform_stack_pop(&n->world->game->tstack);
}

static void
update(struct neon *n)
{
	// Set velocity according to actual direction
	if (n->right)
		n->body->velocity.x = n->speed;
	else
		n->body->velocity.x = -n->speed;

	// Handle collisions with obstacles
	if (physics_body_collision(n->body, WALL_LEFT, IS_COLLIDING))
		n->right = false;
	else if (physics_body_collision(n->body, WALL_RIGHT, IS_COLLIDING))
		n->right = true;

	// Handle collisions with screen borders
	vec2i_t local_pos = world_get_pos_in_location(n->body->position);

	if (local_pos.x < 20.0f) {
		n->right = true;
	} else if (local_pos.x > 300.0f)
		n->right = false;
}

static void
collide_with_player(struct physics_body *body_neon, struct physics_body *body_player)
{
	(void)body_neon;
	struct player *player = body_player->user;
	player_kill(player);
}

static void
collide_with_projectile(struct physics_body *body_neon, struct physics_body *body_projectile)
{
	(void)body_neon;
	struct projectile *proj = body_projectile->user;
	projectile_destroy(proj);
}

struct neon *
neon_create(struct world *world, vec2_t global_pos)
{
	struct neon *n = ecalloc(1, sizeof(*n));

	entity_init(n, world, render, update, clean);

	n->body = physics_body_create(world->space);
	n->body->position = global_pos;
	n->body->size = vec2(10.0f, 10.0f);
	n->body->user = n;

	n->speed = 0.5f;
	n->right = true;

	physics_body_body_callback(n->body, collide_with_player, BODY_COLLISION_PLAYER);
	physics_body_body_callback(n->body, collide_with_projectile, BODY_COLLISION_PROJECTILES);

	return n;
}
