#ifndef NEON_H
#define NEON_H

#include "./entity.h"
#include "map/world.h"

struct neon {
	ENTITY_HEADER

	struct physics_body *body;
	float speed;
	bool right;
};

struct neon *neon_create(struct world *world, vec2_t global_pos);

#endif
