#ifndef PUSH_H
#define PUSH_H

#include "./entity.h"
#include "map/world.h"

enum push_direction { PUSH_DIRECTION_HORIZONTAL = 0x01, PUSH_DIRECTION_VERTICAL = 0x02 };

struct push {
	ENTITY_HEADER

	struct physics_body *body;
	vec2i_t location_pos;
	vec2i_t obstacle_pos;
	bool horizontal;
	bool vertical;
	uint8_t cooldown;
};

struct push *push_create(struct world *world, vec2i_t location_pos,
                         vec2i_t obstacle_pos, bool allow_horizontal, bool allow_vertical);

#endif
