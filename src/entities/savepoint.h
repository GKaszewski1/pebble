#ifndef SAVEPOINT_H
#define SAVEPOINT_H

#include "./entity.h"
#include "map/world.h"

struct savepoint {
	ENTITY_HEADER

	struct physics_body *body;

	bool collected;
	unsigned ticks_since_collected;
};

struct savepoint *savepoint_create(struct world *world, vec2_t position);

#endif
