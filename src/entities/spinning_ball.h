#ifndef SPINNING_BALL_H
#define SPINNING_BALL_H

#include "./entity.h"
#include "map/world.h"
#include "physics/physics.h"

struct spinning_ball {
	ENTITY_HEADER

	vec2i_t location_pos;
	vec2i_t obstacle_pos;

	vec2_t ball_pos;
	vec2_t ball_pos_prev;

	vec2_t x_positions[2];

	struct physics_body *body;

	uint32_t ticks;
};

struct spinning_ball *spinning_ball_create(struct world *world, vec2i_t location_pos,
                                           vec2i_t obstacle_pos);

#endif
