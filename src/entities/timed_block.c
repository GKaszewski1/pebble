#include "timed_block.h"

#include <math.h>

#include "map/obstacle_data.h"

static void
render(struct timed_block *t, struct surface *s, float step)
{
	(void)step;

	// NOTE(legalprisoner8140):
	// Temporary animation until someone decides to do it right. 
	// It is quite legible, I think.
	// TODO: replace animation with original one
	struct recti rect = recti(t->pos.x, t->pos.y, OBSTACLE_SCALE * 2, OBSTACLE_SCALE * 2);

	if (t->show) {
		int trans = 128 - (fmodf(t->tick/60.0f, 1.0f) / 2.0f) * 255.0f;
		transformed_fill(&t->world->game->tstack, color_rgba(255, 128, 0, trans), s, rect);
	} else if (t->last_number + 1 == t->number) {
		int trans = (fmodf(t->tick/60.0f, 1.0f) / 2.0f) * 255.0f;
		transformed_fill(&t->world->game->tstack, color_rgba(255, 128, 0, trans), s, rect);
	}
}

static void
update(struct timed_block *t)
{
	t->tick++;

	struct obstacle *ob = world_get_obstacle(t->world, t->pos);
	
	int number = t->tick / 60 % 5;
	if (number == t->number) {
		ob->id = ID_TIMED_BLOCK1 + t->number;
		ob->flags = FLAGS_IS_SOLID;
		t->show = true;
	} else {
		ob->id = ID_NONE;
		ob->flags = FLAGS_NONE;
		t->show = false;
	}
	t->last_number = number;
}

struct timed_block *
timed_block_create(struct world *world, vec2_t global_pos, int number)
{
	struct timed_block *t = ecalloc(1, sizeof(*t));

	entity_init(t, world, render, update, NULL);

	t->number = number;
	t->tick = 0;

	vec2_t pos = global_pos;
	t->pos = pos;

	return t;
}
