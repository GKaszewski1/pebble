#include "turtle.h"

#include <math.h>

#include "physics/collision_groups.h"
#include "player.h"

static void
clean(struct turtle *t)
{
	t->body_t->remove = true;
	t->body_s->remove = true;
}

static void
render(struct turtle *t, struct surface *s, float step)
{
	struct surface *sprite = assets_get_obstacle(ID_TURTLE);

	entity_util_draw_sprite(t->world->game, s, t->body_t, sprite, vec2i(0, -10), step);
}

static void
update(struct turtle *t)
{
	float speed = t->gottagofast ? 2.f : 0.5f;

	t->body_t->velocity.x = t->direction ? speed : -speed;

	if (physics_body_collision(t->body_t, WALL_LEFT, IS_COLLIDING)) {
		t->direction = TURTLE_DIRECTION_LEFT;
	} else if (physics_body_collision(t->body_t, WALL_RIGHT, IS_COLLIDING)) {
		t->direction = TURTLE_DIRECTION_RIGHT;
	}

	// Gravity
	if (t->gottagofast) {
		t->body_t->velocity.y += 0.2f;
	} else {
		t->body_t->velocity.y = 1.5f;
	}

	// Jump!
	if (physics_body_collision(t->body_t, WALL_TOP, IS_COLLIDING) && t->gottagofast) {
		t->body_t->velocity.y -= 3.0f;
	}

	t->body_s->position.x = t->body_t->position.x;
	t->body_s->position.y = t->body_t->position.y - 10.0f;
}

static void
s_collide_with_player(struct physics_body *bp, struct physics_body *bc)
{
	struct turtle *t = (struct turtle *)bp->user;
	struct player *p = (struct player *)bc->user;

	t->body_t->velocity.y = -2.0f;

	t->gottagofast = true;
	p->body->velocity.y = -5.25f;
}

static void
t_collide_with_player(struct physics_body *bp, struct physics_body *bc)
{
	(void)bp;
	struct player *p = (struct player *)bc->user;

	player_kill(p);
}

struct turtle *
turtle_create(struct world *world, vec2_t global_pos)
{
	struct turtle *t = ecalloc(1, sizeof(*t));

	entity_init(t, world, render, update, clean);

	t->body_s = physics_body_create(world->space);
	t->body_s->size = vec2(20.f, 10.f);
	t->body_s->position = global_pos;
	t->body_s->user = t;

	global_pos.y += 10.f;
	t->body_t = physics_body_create(world->space);
	t->body_t->size = vec2(20.f, 10.f);
	t->body_t->position = global_pos;
	t->body_t->user = t;

	t->direction = TURTLE_DIRECTION_LEFT;
	t->gottagofast = false;

	physics_body_body_callback(t->body_t, t_collide_with_player, BODY_COLLISION_PLAYER);

	physics_body_body_callback(t->body_s, s_collide_with_player, BODY_COLLISION_PLAYER);

	return t;
}
