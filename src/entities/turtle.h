#ifndef TURTLE_H
#define TURTLE_H

// NOTE(legalprisoner8140):
// jumping shit

#include "./entity.h"
#include "map/world.h"

enum turtle_direction {
	TURTLE_DIRECTION_LEFT = 0,
	TURTLE_DIRECTION_RIGHT = 1
};

struct turtle {
	ENTITY_HEADER

	/// Turtle's body
	struct physics_body *body_t;
	
	/// Shell's body
	struct physics_body *body_s;

	enum turtle_direction direction;
	bool gottagofast;
	bool fall;
	float personal_velocity;
};

struct turtle *turtle_create(struct world *world, vec2_t global_pos);

#endif
