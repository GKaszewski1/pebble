#include "./util.h"

#include "common/graphics/surface.h"
#include "common/graphics/transform.h"

void
entity_util_draw_sprite(struct game *game, struct surface *dest, struct physics_body *body,
                        struct surface *sprite, const vec2i_t offset, float step)
{
	// prevent the sprite from spazzing out by rounding his position
	// for some reason if you don't do this he tends to vibrate in place by 1px
	// probably due to float truncation or something
	vec2_t bposition = physics_body_interpolated_position(body, step);
	bposition.x += 0.01f;
	bposition.y += 0.01f;

	vec2i_t p = {
	    .x = bposition.x + offset.x,
	    .y = bposition.y + offset.y,
	};

	transformed_blit(&game->tstack, sprite, dest, p);
}

void
entity_draw_hp_bar(int hp, struct surface *dest)
{
	struct recti bar_pos;
	bar_pos = recti(0, 0, 100, 10);
	surface_fill(dest, &bar_pos, 0, 0, 0, 255);

	bar_pos = recti(1, 1, hp - 2, 8);
	surface_fill(dest, &bar_pos, 255, 255, 0, 255);
}
