#include "vanish.h"

#include <math.h>

#include "./util.h"
#include "common/util.h"
#include "map/location.h"
#include "map/obstacle_data.h"
#include "map/world.h"
#include "physics/collision_groups.h"

#define FADING_TICKS    30
#define INVISIBLE_TICKS 70

#define T0_FADEOUT_START 0
#define T1_FADEOUT_END   FADING_TICKS
#define T2_FADEIN_START  (T1_FADEOUT_END + INVISIBLE_TICKS)
#define T3_FADEIN_END    (T2_FADEIN_START + FADING_TICKS)

static inline float
get_opacity(struct vanish *v)
{
	return fmax(fabs((float)v->timer - (int)(T3_FADEIN_END / 2)) - (int)(INVISIBLE_TICKS / 2),
	            0);
}

static void
render(struct vanish *v, struct surface *s, float step)
{
	float opacity = get_opacity(v) / FADING_TICKS * 255;

	struct surface *sprite = assets_get_obstacle(ID_VANISH);

	surface_set_blending_mode(sprite, SURFACE_BLENDING_MODE_BLEND);

	// Alpha blend
	surface_set_alpha(sprite, opacity);
	entity_util_draw_sprite(v->world->game, s, v->body, sprite, vec2i(1, 1), step);
}

static void
update(struct vanish *v)
{
	struct location *loc = world_get_location(v->world, v->location_pos);
	if (!loc) {
		v->remove = true;
		return;
	}

	if (v->timer > 0)
		++v->timer;

	if (v->timer >= T3_FADEIN_END)
		v->timer = 0;

	struct obstacle *o = location_get_obstacle(loc, v->obstacle_pos);

	if (v->timer >= T1_FADEOUT_END && v->timer <= T2_FADEIN_START) {
		o->flags = FLAGS_NONE;
	} else {
		o->flags = FLAGS_IS_SOLID | FLAGS_CASTS_SHADOW;
	}
}

static void
clean(struct vanish *v)
{
	v->body->remove = true;
}

static void
collide_with_player(struct physics_body *bv, struct physics_body *bplayer)
{
	(void)bplayer;

	struct vanish *v = (struct vanish *)bv->user;

	if (v->timer == 0)
		v->timer = 1;

	if (v->timer > T1_FADEOUT_END)
		v->timer = T1_FADEOUT_END;
}

struct vanish *
vanish_create(struct world *world, vec2i_t location_pos, vec2i_t obstacle_pos)
{
	struct vanish *v = ecalloc(1, sizeof(*v));

	entity_init(v, world, render, update, clean);

	v->body = physics_body_create(world->space);

	vec2_t global_pos = world_get_global_pos(location_pos, obstacle_pos);

	// 1 pixel padding (!!!)
	v->body->position = sub2f(global_pos, 1);
	v->body->size = vec2(12, 12);
	v->body->user = v;
	v->location_pos = location_pos;
	v->obstacle_pos = obstacle_pos;
	physics_body_body_callback(v->body, collide_with_player, BODY_COLLISION_PLAYER);

	return v;
}
