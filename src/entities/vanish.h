#ifndef VANISH_H
#define VANISH_H

#include "./entity.h"
#include "map/world.h"

struct vanish {
	ENTITY_HEADER

	struct physics_body *body;
	vec2i_t location_pos;
	vec2i_t obstacle_pos;
	unsigned timer;
};

struct vanish *vanish_create(struct world *world, vec2i_t location_pos,
                             vec2i_t obstacle_pos);

#endif
