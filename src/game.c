#include "game.h"

#include "stb/stb_ds.h"

#include "backends/audio/audio.h"
#include "backends/render/render.h"
#include "build_config.h"
#include "sound_system.h"

// void input_poll_events(struct input *input);

struct game *
game_init(int flags)
{
	struct game *game = ecalloc(1, sizeof(struct game));

	// adjust size to display
	int width = 320;
	int height = 180;
	if (g_config.window_size > 0) {
		width *= g_config.window_size;
		height *= g_config.window_size;
	} else {
		if (adjust_size(&width, &height) < 0) {
			LOG_ERROR("Failed to adjust resolution");
			exit(1);
		}
	}

	int window_flags = 0;

	if (g_config.vsync) {
		window_flags |= WINDOW_FLAGS_VSYNC;
	}

	struct window *window = window_create("pebble (" VERSION ")", width, height, window_flags);
	input_init(&game->input);

	game->screen = surface_create_empty(320, 180, SURFACE_PIXEL_FORMAT_RGB24);

	// Create audio context
	game->audio_ctx = audio_context_create();
	// Create sound system
	game->sound_sys = sound_system_create(game->audio_ctx);
	game->window = window;
	game->running = true;
	game->states = NULL;
	game->flags = flags;
	game->gamesave = NULL;
	transform_stack_init(&game->tstack);

	int assets_flags = ASSETS_NONE;
	// We gonna use gamedata, yay
	if (flags & GAME_USE_GAMEDATA)
		assets_flags = ASSETS_GAMEDATA;
	assets_init(g_config.assets_dir, game, assets_flags);

	return game;
}

void
game_free(struct game **_game)
{
	if (!_game)
		return;

	struct game *game = *_game;

	// Pop all states
	while (game_pop_state(game, NULL))
		;

	assets_clean();
	surface_clean(&game->screen);
	window_free(game->window);
	game_save_clean(game->gamesave);
	sound_system_clean(&game->sound_sys);
	audio_context_clean(&game->audio_ctx);

	free(game);

	*_game = NULL;
}

void
game_handle_events(struct game *game)
{
	input_poll_events(&game->input, game->window);
}

void
game_update(struct game *game)
{
	++game->total_ticks;

	struct state *st = arrlast(game->states);
	assert(st != NULL);

	st->update(game);
	input_tick(&game->input);
}

void
game_draw(struct game *game, float step)
{
	struct state *st = arrlast(game->states);
	assert(st != NULL);

	st->draw(game, step);
	window_render(game->window, game->screen);
}

bool
game_running(struct game *game)
{
	return game->running;
}

void
game_stop(struct game *game)
{
	assert(game != NULL);
	game->running = false;
}

void
game_change_state(struct game *game, struct state *state)
{
	assert(state != NULL);
	game_pop_state(game, NULL);
	game_push_state(game, state);
}

void
game_push_state(struct game *game, struct state *state)
{
	assert(state != NULL);

	// Pause last state
	if (arrlen(game->states)) {
		arrlast(game->states)->pause();
	}

	// Push state
	arrput(game->states, state);
	state->init(game);
}

bool
game_pop_state(struct game *game, void *data)
{
	bool popped = false;

	// Remove last state
	if (arrlen(game->states)) {
		struct state *st = arrpop(game->states);
		assert(st != NULL);

		// Call clean function
		st->clean();

		// Remove state object
		state_clean(&st);
		popped = true;

		if (arrlen(game->states) == 0) {
			arrfree(game->states);
			game->states = NULL;
		}
	}

	// Resume last state
	if (arrlen(game->states)) {
		struct state *st = arrlast(game->states);
		assert(st != NULL);
		st->resume(data);
	}

	return popped;
}
