#include "game_save.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "common/data.h"
#include "common/log.h"
#include "common/util.h"
#include "misc/config.h"
#include "sys/fs.h"

struct game_save *
game_save_create_empty()
{
	struct game_save *gs = ecalloc(1, sizeof(*gs));
	return gs;
}

char *
game_save_get_path(const char *filename)
{
	char *path = NULL;

	if (g_config.data_path) {
		path = dstrcpy(g_config.data_path);
	} else {
		path = get_data_path();
	}

	char *final = dsprintf("%s%s", path, filename);
	free(path);

	return final;
}

struct game_save *
game_save_load_file(const char *path)
{
	struct game_save *gs;
	FILE *gs_file;
	uint32_t file_header;
	uint32_t respawn_pos_x;
	uint32_t respawn_pos_y;

	gs = ecalloc(1, sizeof(*gs));

	gs_file = fopen(path, "rb");

	if (gs_file == NULL) {
		LOG_ERRORF("Failed to open game save file `%s`", path);

		free(gs);
		return NULL;
	}

	file_header = fdeserialize_u32(gs_file);
	if (file_header != GAME_SAVE_FILE_HEADER) {
		LOG_ERROR("Provided game save file is not valid");

		free(gs);
		fclose(gs_file);
		return NULL;
	}

	respawn_pos_x = fdeserialize_u32(gs_file);
	respawn_pos_y = fdeserialize_u32(gs_file);
	gs->checkpoint_position = (vec2i_t){respawn_pos_x, respawn_pos_y};
	gs->max_bullets = fdeserialize_u8(gs_file);
	gs->keys_collected = fdeserialize_u8(gs_file);
	gs->kolota_defeated = fdeserialize_u8(gs_file);

	fclose(gs_file);
	return gs;
}

void
game_save_write_file(struct game_save *gs, const char *filename)
{
	FILE *gs_file;

	gs_file = fopen(filename, "wb");

	if (gs_file == NULL) {
		LOG_ERRORF("Failed to open game save file `%s`", filename);

		game_save_clean(gs);
		fclose(gs_file);
		return;
	}

	fserialize_u32(gs_file, GAME_SAVE_FILE_HEADER);
	fserialize_u32(gs_file, gs->checkpoint_position.x);
	fserialize_u32(gs_file, gs->checkpoint_position.y);
	fserialize_u8(gs_file, gs->max_bullets);
	fserialize_u8(gs_file, gs->keys_collected);
	fserialize_u8(gs_file, gs->kolota_defeated);

	fclose(gs_file);

	LOG_INFO("Saved game save to file");
}

void
game_save_clean(struct game_save *gs)
{
	if (gs != NULL)
		free(gs);
}
