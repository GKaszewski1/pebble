#include "backends/main/main.h"

#include <stdio.h>
#include <time.h>

#include "assets.h"
#include "backends/mixed/messagebox.h"
#include "backends/render/render.h"
#include "common/log.h"
#include "common/timing.h"
#include "game.h"
#include "gamedata.h"
#include "misc/config.h"
#include "states/editor.h"
#include "states/languageselect.h"
#include "states/play.h"

int
main(int argc, char *argv[])
{
	(void)argc;
	(void)argv;

	srand(time(NULL));

	// If config is not found then default values will be used.
	parse_config("config.ini", &g_config);

	time_init();

	int flags = GAME_NONE;
	if (g_config.gamedata)
		flags |= GAME_USE_GAMEDATA;

	struct game *game = game_init(flags);

	game_change_state(game, languageselect_create());

	const double update_frequency = 60;
	const double seconds_per_update = 1 / update_frequency;

	double previous = time_get_seconds();
	double lag = 0.0;

	uint32_t ticks = 0;

	while (game_running(game)) {
		double current = time_get_seconds();
		double elapsed = current - previous;
		previous = current;
		lag += elapsed;

		game_handle_events(game);

		while (lag >= seconds_per_update) {
			game_update(game);
			ticks++;
			if (ticks % 60 == 0) {
				assets_gc();
			}
			lag -= seconds_per_update;
		}

		float step = lag / seconds_per_update;
		game_draw(game, step);
	}

	game_free(&game);
	return 0;
}
