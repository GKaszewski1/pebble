/// @addtogroup location
/// @{

#ifndef LOCATION_PROCESSOR_H
#define LOCATION_PROCESSOR_H

#include "./world.h"

/// @brief Create processed location loaded from gamedata files
/// with entity creation and block modification procedure.
/// @param world world.
/// @param location_pos the location to load and process.
/// @param refresh Hint to location processor if location is being refreshed
/// @returns newly allocated and preprocessed location
struct location *location_load_process(struct world *world, vec2i_t location_pos,
                                       bool refresh);

#endif

/// @}
