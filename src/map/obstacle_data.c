#include "obstacle_data.h"

void
get_flags_for_obstacle_id(uint16_t id, bool *solid, bool *visible, bool *casts_shadow)
{
	*solid = true;
	switch (id) {
	case 0:
	case 16:
	case 17:
	case 18:
	case 19:
	case 20:
	case 21:
	case 22:
	case 23:
	case 24:
	case 25:
	case 26:
	case 28:
	case 29:
	case 30:
	case 31:
	case 32:
	case 42:
	case 43:
	case 44:
	case 45:
	case 46:
	case 47:
	case 48:
	case 49:
	case 50:
	case 51:
	case 58:
	case 59:
	case 60:
	case 61:
	case 62:
	case 63:
	case 79:
	case 80:
	case 82:
	case 83:
	case 84:
	case 85:
	case 94:
	case 95:
		*solid = false;
		break;
	case 96:
	case 97:
		*solid = false;
		// b->id = 0;
		break;
	case 98:
	case 99:
	case 100:
	case 101:
		*solid = false;
		break;
	case 102:
	case 103:
		*solid = false;
		// b->id = 0;
		break;
	case 104:
	case 105:
	case 106:
		*solid = false;
		break;
	case 117:
		*solid = false;
		// if(opened_key >= 1) b->id = 237;
		break;
	case 118:
		*solid = false;
		// if(opened_key >= 2) b->id = 237;
		break;
	case 119:
		*solid = false;
		// if(opened_key >= 3) b->id = 237;
		break;
	case 120:
		*solid = false;
		// if(opened_key >= 4) b->id = 237;
		break;
	case 121:
		*solid = false;
		// if(opened_key >= 5) b->id = 237;
		break;
	case 122:
		*solid = false;
		// if(opened_key >= 6) b->id = 237;
		break;
	case 123:
		*solid = false;
		// if(opened_key >= 7) b->id = 237;
		break;
	case 124:
		*solid = false;
		// if(opened_key >= 8) b->id = 237;
		break;
	case 125:
		*solid = false;
		// if(opened_key >= 9) b->id = 237;
		break;
	case 126:
		*solid = false;
		// if(opened_key >= 10) b->id = 237;
		break;
	case 127:
	case 128:
		*solid = false;
		// b->id = 0;
		break;
	case 135:
	case 136:
	case 137:
	case 138:
	case 139:
	case 140:
		*solid = false;
		break;
	case 141:
		*solid = false;
		// b->id = 0;
		break;
	case 142:
		*solid = false;
		// b->id = 104;
		break;
	case 152:
	case 153:
	case 154:
	case 155:
	case 156:
	case 157:
	case 158:
	case 159:
		*solid = false;
		break;
	case 160:
	case 161:
	case 162:
	case 169:
		*solid = false;
		// b->id = 0;
		break;
	case 170:
		*solid = false;
		break;
	case 171:
		*solid = true;
		// b->id = 0;
		break;
	case 172:
		*solid = true;
		// b->id = 0;
		break;
	case 189:
	case 190:
	case 191:
	case 192:
	case 193:
	case 194:
		*solid = false;
		// b->id = 0;
		break;
	case 195:
	case 196:
	case 197:
	case 198:
	case 199:
	case 200:
		*solid = false;
		break;
	case 201:
	case 203:
	case 204:
	case 205:
	case 206:
	case 207:
		*solid = false;
		// b->id = 0;
		break;
	case 208:
	case 209:
	case 210:
	case 211:
	case 212:
	case 213:
	case 214:
	case 217:
	case 218:
	case 219:
	case 220:
	case 221:
	case 222:
	case 223:
	case 226:
	case 227:
	case 228:
	case 229:
	case 230:
	case 231:
	case 232:
	case 233:
		*solid = false;
		break;
	case 234:
	case 235:
	case 236:
		*solid = false;
		// b->id = 0;
		break;
	case 237:
		*solid = false;
		break;
	case 238:
		*solid = false;
		// b->id = 0;
		break;
	case 259:
	case 260:
	case 261:
	case 262:
	case 263:
	case 264:
	case 265:
	case 266:
	case 267:
	case 268:
	case 269:
	case 270:
	case 271:
	case 272:
	case 273:
	case 274:
	case 275:
	case 277:
		*solid = false;
		break;
	}

	*visible = true;
	switch (id) {
	case 104:
	case 195:
	case 196:
	case 197:
	case 198:
	case 199:
	case 201:
		*visible = false;
		break;
	}

	*casts_shadow = *solid;
	switch (id) {
	case 0:
	case 5:
	case 6:
	case 7:
	case 8:
	case 9:
	case 52:
	case 53:
	case 54:
	case 55:
	case 56:
	case 57:
	case 129:
	case 130:
	case 131:
	case 132:
	case 133:
	case 134:
	case 173:
	case 174:
	case 175:
	case 176:
	case 177:
	case 178:
	case 179:
	case 180:
	case 181:
	case 182:
	case 183:
	case 184:
	case 185:
	case 186:
	case 187:
	case 188:
	case 215:
	case 216:
	case 224:
	case 225:
	case 275:
		*casts_shadow = false;
		break;
	case 85:
		*casts_shadow = true;
		break;
	}
}
