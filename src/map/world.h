/// @addtogroup world World
/// @{

#ifndef WORLD_H
#define WORLD_H

#include <stdbool.h>
#include <stdint.h>

#include "common/math/vector.h"

struct surface;

struct world {
	struct game *game;
	struct physics_space *space;

	struct audio_music *playing_music;
	int playing_music_id;

	struct entity **entities;    // stb arr
	struct particle **particles; // stb arr

	// Player entity list
	// Observer pointers of entities array
	struct player **players; // stb arr

	struct location **loaded_locations; // stb arr

	vec2i_t previous_loc_pos;
	vec2i_t current_loc_pos;

	float loc_pos_change_bg_fade; // Used for background change animation

	vec2i_t camera_pos;

	uint8_t keys_collected;
};

/// @brief Creates and initializes a new, blank world.
///
/// @return pointer to the world or NULL on failure
struct world *world_create(struct game *game);

/// @brief Frees a world and all associated resources.
///
/// @param world the world to clean up.
void world_clean(struct world **world);

/// @brief Ticks the world.
///
/// @param world the world to tick
void world_update(struct world *world);

/// @brief Renders the world.
///
/// @param world World to render
/// @param target Target surface to draw
/// @param step Step between the current and next frame
void world_render(struct world *world, struct surface *target, float step);

/// @brief Spawns an entity into the world.
/// @param ent the entity pointer to spawn in, freed automatically
void world_spawn_entity(struct world *world, void *ent);

/// @brief Spawns a particle into the world
/// @param particle the particle pointer to spawn in, freed automatically
void world_spawn_particle(struct world *world, void *particle);

/// @brief Goes to the location at the given coordinates.
///
/// The switch only happens if the location changed between the previous and
/// current call to the function.
void world_go_to(struct world *world, vec2i_t loc_pos);

/// @brief Returns obstacle from global position.
/// @param global_pos Global position
/// @returns Obstacle from loaded locations or NULL on failure.
/// @note Returns NULL during location transition animations.
struct obstacle *world_get_obstacle(struct world *world, vec2_t global_pos);

/// @brief Returns location from location position.
/// @param location_pos Location position
/// @return Location from loaded locations or NULL on failure.
struct location *world_get_location(struct world *world, vec2i_t location_pos);

/// @brief Loads a location that is at the given position.
/// @param location_pos Location position
/// @return Pointer to loaded location or NULL if location doesn't exists.
struct location *world_load_location(struct world *world, vec2i_t location_pos);

/// @brief Unloads a location.
/// @param location_pos Location position
/// @returns True if found and unloaded
bool world_unload_location(struct world *world, vec2i_t location_pos);

/// @brief transform_stack_get_translation() wrapper
/// @returns Screen position relative to camera
/// @param position Global position
/// @see transform_stack_get_translation()
vec2i_t world_get_visual_pos(struct world *world, vec2_t global_pos);

/// @brief Transforms a location and obstacle position pair into a global position. 
/// @returns Global position from location & obstacle position
vec2_t world_get_global_pos(vec2i_t location_pos, vec2i_t obstacle_pos);

/// @brief Transforms a location and obstacle position pair into a global position. 
/// @returns Global position from location & obstacle position
vec2i_t world_get_global_posi(vec2i_t location_pos, vec2i_t obstacle_pos);

/// @brief transform_stack_get_translation() wrapper
/// @returns screen position relative to camera
/// @param location_pos Location coordinates
/// @param obstacle_pos Obstacle local (relative to location) coordinates, in blocks
/// @see transform_stack_get_translation()
vec2i_t world_get_visual_pos2(struct world *world, vec2i_t location_pos,
                                   vec2i_t obstacle_pos);

/// @brief Transforms a global position pair into a position in location. 
/// @returns Location position from global position.
vec2i_t world_get_pos_in_location(vec2_t global_pos);

/// @brief Transforms a global position pair into a rounded obstacle position. 
/// @returns Obstacle position from global position.
vec2i_t world_get_obstacle_pos_round(vec2_t global_pos);

/// @}

#endif
