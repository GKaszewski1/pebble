#ifndef CONFIG_H
#define CONFIG_H

#include <assert.h>
#include <inih/ini.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common/log.h"
#include "common/util.h"

enum lang { LANG_ENGLISH, LANG_POLISH };

enum {
	PHYSICS_VERBOSE_ENTITIES = 0x1,
	PHYSICS_VERBOSE_TILES = 0x2
};

struct conf {
	/// Default: 0
	int window_size;
	/// Null if path is invalid
	char *assets_dir;
	/// Default: true
	bool vsync;
	/// Default: null
	char *data_path;
	/// Default: null
	char *gamedata;

	int language;
	int physics_verbose;
	bool stats;
	bool disable_shadows;
};

extern struct conf g_config;

int parse_config(const char *path, struct conf *conf);

#endif
