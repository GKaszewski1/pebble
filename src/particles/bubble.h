#ifndef PART_BUBBLE_H
#define PART_BUBBLE_H

#include "particle.h"

struct particle_bubble {
	PARTICLE_HEADER

	vec2_t velocity;
	uint16_t lifetime;
	bool single_dot;
};

struct particle_bubble *particle_bubble_create(struct world *world, vec2_t position,
                                               vec2_t velocity);

#endif
