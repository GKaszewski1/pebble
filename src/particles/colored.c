#include "colored.h"

#include "map/world.h"

static void
render(struct particle_colored *part, float step)
{
	(void)step;

	vec2i_t pos = world_get_visual_pos(part->world, part->position);

	surface_set_pixel_safe(part->world->game->screen, pos.x, pos.y, part->red, part->green,
	                       part->blue, 255);
}

static void
update(struct particle_colored *part)
{
	part->lifetime--;
	if (part->lifetime == 0) {
		part->remove = true;
		return;
	}

	part->position.x += part->velocity.x;
	part->position.y += part->velocity.y;

	bool center, left, right, top, bottom;
	particle_collision_get_flags(part->world, part->position, 3, &center, &left, &right, &top,
	                             &bottom);

	if (left && right && bottom && top) { // stuck inside block
		part->remove = true;
		return;
	}

	if (!center && !part->bouncing) {
		// Resume bouncing
		part->bounce_count = 0;
		part->bouncing = true;
	}

	if (!part->bouncing) {
		return;
	}

	particle_bounce_physics_tick(&part->velocity, center, left, right, top, bottom,
	                             &part->bounce_count, 0.4f, 0.8f);

	if (part->bounce_count > 10) {
		part->velocity = vec2(0.0f, 0.0f);
		part->bouncing = false;
	}
}

struct particle_colored *
particle_colored_create(struct world *world, vec2_t position, vec2_t velocity,
                        uint8_t red, uint8_t green, uint8_t blue)
{
	struct particle_colored *part = ecalloc(1, sizeof(struct particle_colored));

	particle_init(part, world, position, render, update, NULL);

	part->velocity = velocity;
	part->lifetime = 100 + rand() % 100;
	part->red = red;
	part->green = green;
	part->blue = blue;
	part->bouncing = true;

	return part;
}
