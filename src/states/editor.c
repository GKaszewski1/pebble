#include "editor.h"

#include "common/math/vector.h"

enum editorstate_action {
	PLACE_OBSTACLE,
	COPY_OBSTACLE,
	REMOVE_OBSTACLE,
};

struct editorstate {
	struct location *loc;
	int ob_id;
	struct game *game;
};

struct editorstate *editor_state = NULL;

static void
quit()
{
	if (editor_state->game)
		game_stop(editor_state->game);
}

void
editorstate_init(struct game *game)
{
	editor_state = ecalloc(1, sizeof(*editor_state));

	struct location *loc = location_create();
	loc->pos.x = 0;
	loc->pos.y = 0;
	location_set_background(loc, 20);
	// 	location_set_obstacle(
	// 	    loc, (struct obstacle){.id = 1, .flags = FLAGS_NONE, .x = 0, .y =
	// 0});

	editor_state->loc = loc;
	editor_state->ob_id = 1;
	editor_state->game = game;

	input_on_quit(&game->input, quit);
}

void
editorstate_process_action(struct game *game, enum editorstate_action action)
{
	int ob_x = (int)game->input.mouse.x / OBSTACLE_SCALE;
	int ob_y = (int)game->input.mouse.y / OBSTACLE_SCALE;

	if (ob_x >= LOCATION_WIDTH || ob_x < 0 || ob_y >= LOCATION_HEIGHT || ob_y < 0)
		return;

	switch (action) {
	case PLACE_OBSTACLE: {
		struct obstacle obstacle =
		    (struct obstacle){.id = editor_state->ob_id, .flags = FLAGS_IS_SOLID};

		location_set_obstacle(editor_state->loc, vec2i(ob_x, ob_y), obstacle);
		break;
	}

	case REMOVE_OBSTACLE: {
		struct obstacle obstacle = (struct obstacle){.id = 0, .flags = FLAGS_NONE};

		location_set_obstacle(editor_state->loc, vec2i(ob_x, ob_y), obstacle);
		break;
	}

	case COPY_OBSTACLE:
		// dereference cannot occur
		editor_state->ob_id = location_get_obstacle(editor_state->loc, vec2i(ob_x, ob_y))->id;
		break;
	}
}

void
editorstate_clean()
{
}

void
editorstate_pause()
{
}

void
editorstate_resume()
{
}

void
editorstate_handle_events(struct game *game)
{
	(void)game;
}

void
editorstate_update(struct game *game)
{
	// key actions
	if (input_key(&game->input, KEY_ESC, STATE_JUST_PRESSED)) {
		// location_save(editor_state->loc, "assets/locations/0,0.pbmap");
		game_pop_state(editor_state->game, NULL);
	}

	// mouse actions
	if (input_mouse_button(&game->input, MOUSE_BUTTON_LEFT, STATE_IS_DOWN)) {
		editorstate_process_action(game, PLACE_OBSTACLE);
	}
	if (input_mouse_button(&game->input, MOUSE_BUTTON_RIGHT, STATE_IS_DOWN)) {
		editorstate_process_action(game, REMOVE_OBSTACLE);
	}
	if (input_mouse_button(&game->input, MOUSE_BUTTON_MIDDLE, STATE_IS_DOWN)) {
		editorstate_process_action(game, COPY_OBSTACLE);
	}

	// obstacle selection
	editor_state->ob_id += (int)game->input.wheel.y;
	editor_state->ob_id = clampi(editor_state->ob_id, 0, OBSTACLES_COUNT);
}

void
editorstate_draw(struct game *game, float step)
{
	(void)step;

	// render location
	location_render_background(editor_state->loc, game->screen, 0, 0, 255);
	location_render(editor_state->loc, game->screen, 0, 0);
}

struct state *
editorstate_create()
{
	return state_create((init_fn)editorstate_init, (clean_fn)editorstate_clean,
	                    (pause_fn)editorstate_pause, (resume_fn)editorstate_resume,
	                    (update_fn)editorstate_update, (draw_fn)editorstate_draw);
}
