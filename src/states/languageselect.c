#include "languageselect.h"

#include "joystick_binds.h"

struct languageselect {
	struct recti pos[2];
	uint8_t current;

	struct game *game;
};

struct languageselect *languageselect_state = NULL;

static void
quit()
{
	if (languageselect_state->game)
		game_stop(languageselect_state->game);
}

void
languageselect_init(struct game *game)
{
	languageselect_state = emalloc(sizeof(*languageselect_state));
	if (languageselect_state != NULL) {
		languageselect_state->game = game;

		languageselect_state->pos[0] = (struct recti){.x = 0, .y = 79};
		languageselect_state->pos[1] = (struct recti){.x = 0, .y = 93};

		languageselect_state->current = 0;

		// Register event callbacks
		input_on_quit(&game->input, quit);
	}
	joystick_set_binds_menu(&game->input);
}

void
languageselect_clean()
{
	free(languageselect_state);
}

void
languageselect_pause()
{
}

void
languageselect_resume()
{
	input_on_quit(&languageselect_state->game->input, quit);
}

void
languageselect_update(struct game *game)
{
	assert(game != NULL);

	if (input_key(&game->input, KEY_UP, STATE_JUST_PRESSED)) {
		languageselect_state->current = 0;
	}
	if (input_key(&game->input, KEY_DOWN, STATE_JUST_PRESSED)) {
		languageselect_state->current = 1;
	}

	if (input_key(&game->input, KEY_ENTER, STATE_JUST_PRESSED)) {
		g_config.language = languageselect_state->current;
		game_push_state(languageselect_state->game, menustate_create());
	}
}

void
languageselect_draw(struct game *game, float step)
{
	(void)step;

	assert(game != NULL);

	int la_x = 151;
	int la_y = languageselect_state->pos[languageselect_state->current].y;

	// Draw all of it
	surface_blit(g_assets.langselect, NULL, game->screen, vec2i(0, 0));
	surface_blit(g_assets.langarrow, NULL, game->screen, vec2i(la_x, la_y));
}

struct state *
languageselect_create()
{
	return state_create((init_fn)languageselect_init, (clean_fn)languageselect_clean,
	                    (pause_fn)languageselect_pause, (resume_fn)languageselect_resume,
	                    (update_fn)languageselect_update, (draw_fn)languageselect_draw);
}
