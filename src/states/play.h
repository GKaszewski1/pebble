#ifndef GAME_STATE_H
#define GAME_STATE_H

#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "assets.h"
#include "backends/render/render.h"
#include "common/log.h"
#include "game.h"
#include "map/world.h"
#include "misc/config.h"
#include "pause_play.h"
#include "states.h"

enum { PLAYSTATE_NONE = 0x0, PLAYSTATE_CONTINUE = 0x1 };

struct playstate;

struct state *playstate_create(int flags);

#endif
