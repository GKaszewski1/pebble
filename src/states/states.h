#ifndef STATES_H
#define STATES_H

#include <stdlib.h>

#include "backends/render/render.h"
#include "common/log.h"
#include "misc/config.h"

struct game;

typedef void (*init_fn)(struct game *game);
typedef void (*clean_fn)();
typedef void (*pause_fn)();
typedef void (*resume_fn)(void *data);
typedef void (*update_fn)(struct game *game);
typedef void (*draw_fn)(struct game *game, float step);

struct state {
	init_fn init;
	clean_fn clean;
	pause_fn pause;
	resume_fn resume;
	update_fn update;
	draw_fn draw;
};

/// @brief Create new state
struct state *state_create(init_fn init, clean_fn clean, pause_fn pause, resume_fn resume,
                           update_fn update, draw_fn draw);

/// @brief Free state
/// @param state state to remove
void state_clean(struct state **state);

#endif
