#include <stdio.h>
#include "parg/parg.h"

#include "common/string.h"
#include "common/util.h"
#include "sys/fs.h"

#define MAKE_GD_FILE_PUBLIC
#include "gamedata.h"

#define IS_NOT(name, first, second) \
	(!(strcmp(name, first) == 0) && !(strcmp(name, second) == 0))

#define eprint(msg)       fputs(msg "\n", stderr);
#define eprintf(msg, ...) fprintf(stderr, msg "\n", __VA_ARGS__);

int
pack(const char *assets_path)
{
	if (!fs_is_directory(assets_path)) {
		return 1;
	}

	char **files = NULL;
	char **dirs = NULL;
	arrput(dirs, assets_path);

	for (size_t i = 0; i < arrlenu(dirs); i++) {
		struct fs_file *f;
		struct fs_dir *dir = fs_walk_start(dirs[i]);

		while ((f = fs_walk_dir(dir)) != NULL) {
			if (IS_NOT(f->name, ".", "..")) {
				if (f->type == FS_IS_FILE)
					arrput(files, dsprintf("%s/%s", dirs[i], f->name));
				else if (f->type == FS_IS_DIRECTORY)
					arrput(dirs, dsprintf("%s/%s", dirs[i], f->name));
			}
		}

		fs_walk_end(dir);
	}

	struct gd_file *gamedata_files = emalloc(arrlenu(files) * sizeof(struct gd_file));
	for (size_t i = 0; i < arrlenu(files); i++) {
		gamedata_files[i].path = files[i];

		FILE *f = fopen(files[i], "rb");

		fseek(f, 0, SEEK_END);
		size_t size = ftell(f);
		fseek(f, 0, SEEK_SET);

		gamedata_files[i].content.size = size;
		gamedata_files[i].content.data = emalloc(size * sizeof(uint8_t));
		fread(gamedata_files[i].content.data, sizeof(uint8_t), size, f);

		fclose(f);
	}

	gamedata_save(gamedata_files, arrlenu(files), "gamedata");

	return 0;
}

int
list(const char *gamedata_path)
{
	if (!fs_is_file(gamedata_path)) {
		return 1;
	}

	struct gamedata gd;
	int ret = gamedata_from_file(&gd, GD_MODE_STATIC, gamedata_path);

	switch (ret) {
	case GD_OK:
		break;
	case GD_CANNOT_OPEN_FILE:
		eprint("error: cannot open file (doesn't exists?)");
		return 1;
	case GD_FILE_CORRUPTED:
		eprint("error: gamedata is corrupted");
		break;
	}

	// Print every file in gamedata
	for (size_t i = 0; i < hmlenu(gd.files); i++) {
		struct file_pair f = gd.files[i];
		printf("%s\n", f.key);
	}

	return 0;
}

void
print_help()
{
	eprint("Usage: packer [-h] [-p | -l] <path>");
}

int
main(int argc, char *argv[])
{
	struct parg_state ps;
	int c;

	parg_init(&ps);

	while ((c = parg_getopt(&ps, argc, argv, "hp:l:")) != -1) {
		switch (c) {
		case 1:
			break;

		case 'h': // help
			print_help();
			return 0;

		case 'p': // pack
			return pack(ps.optarg);

		case 'l': // list
			return list(ps.optarg);

		case '?': // unknown option or
			if (ps.optopt == 'p') {
				eprint("option -p requires a path to assets directory");
			} else if (ps.optopt == 'l') {
				eprint("option -s requires a gamedata");
			} else {
				eprintf("unknown option -%c", ps.optopt);
			}

			return 1;

		default:
			eprintf("error: unhandled option -%c", c);
			return 1;
		}
	}

	print_help();

	return 0;
}
